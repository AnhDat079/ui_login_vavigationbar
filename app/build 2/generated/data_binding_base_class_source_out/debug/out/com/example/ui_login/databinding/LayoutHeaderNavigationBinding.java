// Generated by view binder compiler. Do not edit!
package com.example.ui_login.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.ui_login.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class LayoutHeaderNavigationBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final TextView txtName;

  @NonNull
  public final TextView txtPhone;

  private LayoutHeaderNavigationBinding(@NonNull LinearLayout rootView, @NonNull TextView txtName,
      @NonNull TextView txtPhone) {
    this.rootView = rootView;
    this.txtName = txtName;
    this.txtPhone = txtPhone;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static LayoutHeaderNavigationBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static LayoutHeaderNavigationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.layout_header_navigation, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static LayoutHeaderNavigationBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.txtName;
      TextView txtName = ViewBindings.findChildViewById(rootView, id);
      if (txtName == null) {
        break missingId;
      }

      id = R.id.txtPhone;
      TextView txtPhone = ViewBindings.findChildViewById(rootView, id);
      if (txtPhone == null) {
        break missingId;
      }

      return new LayoutHeaderNavigationBinding((LinearLayout) rootView, txtName, txtPhone);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
