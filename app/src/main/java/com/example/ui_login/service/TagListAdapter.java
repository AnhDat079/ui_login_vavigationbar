package com.example.ui_login.service;

import static com.example.ui_login.until.ActionName.Action_SelectEPC;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.activities.TagRecord;
import com.example.ui_login.R;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class TagListAdapter extends RecyclerView.Adapter<TagListAdapter.TagListViewHolder> {
    private static final String TAG = TagListAdapter.class.getSimpleName();
    private Context context;
    private ReaderActivity activity;

    private ConcurrentHashMap<String, TagRecord> tagDataMapping;
    private ArrayList<String> tagListMapping;
    private int totalTags = 0;

    public TagListAdapter(Context context, ReaderActivity activity) {
        this.context = context;
        this.activity = activity;

        tagDataMapping = new ConcurrentHashMap<>();
        tagListMapping = new ArrayList<>();
    }

    public void add(String tagEPC, int tagRSSI, int count) {
        totalTags += count;

        if (tagDataMapping.containsKey(tagEPC)) {
            TagRecord tempTR = tagDataMapping.get(tagEPC);
            tempTR.setRssi(tagRSSI);
            tempTR.readCount += count;

//            notifyItemChanged(tempTR.getOrder());
            notifyDataSetChanged();
        } else {
            tagListMapping.add(tagEPC);
            TagRecord newTag = new TagRecord();
            newTag.setEpcString(tagEPC);
            newTag.setRssi(tagRSSI);
            newTag.readCount = count;
            newTag.setOrder(tagListMapping.size() - 1);
            tagDataMapping.put(tagEPC, newTag);

            notifyItemInserted(tagListMapping.size() - 1);
        }
    }

    public void add(ConcurrentHashMap<String, TagRecord> tagList) {
        for (TagRecord tag : tagList.values()) {
            add(tag.getEpcString(), tag.getRssi(), tag.getReadCount());
        }
    }

    /**
     * Clear all datas in the adapter
     */
    public void clear() {
        int nRange = tagListMapping.size();
        tagListMapping.clear();
        tagDataMapping.clear();
        totalTags = 0;
        notifyItemRangeRemoved(0, nRange);

    }

    /**
     * Return the list size
     *
     * @return
     */
    public int size() {
        return tagListMapping.size();
    }

    /**
     * Convert the data for save tag
     * @return
     */
    public String toString() {
        StringBuilder tmpTags = new StringBuilder();

        for (int i = 0; i < tagListMapping.size(); i++) {
            tmpTags.append(String.format("%s,%s,%s,%s\r\n", i + 1, tagListMapping.get(i), tagDataMapping.get(tagListMapping.get(i)).getRssi(), tagDataMapping.get(tagListMapping.get(i)).readCount));
        }

        return tmpTags.toString();
    }

    public int getTotalTags() {
        return totalTags;
    }

    @NonNull
    @Override
    public TagListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_list_fragment, parent, false);
        TagListViewHolder holder = new TagListViewHolder(view);
        holder.tagNO = (TextView) view.findViewById(R.id.tag_no);
        holder.tagEPC = (TextView) view.findViewById(R.id.tag_epc);
        holder.tagRSSI = (TextView) view.findViewById(R.id.tag_rssi);
        holder.tagCount = (TextView) view.findViewById(R.id.tag_count);
        holder.context = this.context;

        holder.tagNO.setWidth(activity.tableNumberWidth);
        holder.tagEPC.setWidth(activity.tableEPCWidth);
        holder.tagRSSI.setWidth(activity.tableRSSIWidth);
        holder.tagCount.setWidth(activity.tableCountWidth);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TagListViewHolder holder, int position) {
        if (tagListMapping.size() >= position) {
            TagRecord tag = tagDataMapping.get(tagListMapping.get(position));

            holder.tagNO.setText(String.valueOf(position + 1));
            holder.tagEPC.setText(tag.getEpcString());
            holder.tagRSSI.setText(String.valueOf(tag.getRssi()));
            holder.tagCount.setText(String.valueOf(tag.readCount));
            holder.context = this.context;
        }
    }

    @Override
    public int getItemCount() {
        return tagListMapping == null ? 0 : tagListMapping.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class TagListViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public TextView tagNO;
        public TextView tagEPC;
        public TextView tagRSSI;
        public TextView tagCount;

        public TagListViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("TagListViewHolder", "Element " + getAdapterPosition() + " clicked. EPC : " + tagEPC.getText().toString());

                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Action_SelectEPC).putExtra("epc", tagEPC.getText().toString()));
                }
            });
        }
    }
}


