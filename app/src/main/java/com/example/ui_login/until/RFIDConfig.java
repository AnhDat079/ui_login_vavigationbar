package com.example.ui_login.until;

import android.content.Context;
import android.content.SharedPreferences;

public class RFIDConfig {
    private final int Rapid_Read_Single_Mode = 0;
    private final int Rapid_Read_Multi_Mode = 1;
    private final int Cycle_Count_Mode = 2;
    private final int Custom_Mode = 3;

    private final String Custom_Conf = "Custom";

    private final String Key_Operation_Mode = "OperationMode";
    private final String Key_Reader_Timeout = "ReaderTimeout";
    private final String Key_Antenna_On_Time = "OnTime";
    private final String Key_Antenna_Off_Time = "OffTime";
    private final String Key_Session = "Session";
    private final String Key_Target = "Target";
    private final String Key_Fast_Search = "FastSearch";
    private final String Key_Q_Value = "QValue";
    private final String Key_Encoding = "Encoding";
    private final String Key_TARI = "TARI";
    private final String Key_BLF = "BLF";
    private final String Key_Power = "Power";

    private final int defaultMode = Cycle_Count_Mode;
    private final int defaultReaderTimeout = 1000;
    private final int defaultAntennaOnTime = 300;
    private final int defaultAntennaOffTime = 300;
    private final int defaultPower = 20;
    private final int defaultBLF = 0;
    private final int defaultTARI = 0;
    private final int defaultSession = 1;
    private final int defaultEncoding = 3;
    private final int defaultQ = 0;
    private final int defaultTarget = 2;
    private final boolean defaultFastSearch = false;

    private SharedPreferences properties;

    private static Context appContext;

    public RFIDConfig(Context context) {
        appContext = context;
        loadConf();
    }

    /**
     * Change the setting to default value
     */
    private void setDefault() {

        setOperationMode(defaultMode);

        setReaderTimeout(defaultReaderTimeout);

        setAntennaOnTime(defaultAntennaOnTime);

        setAntennaOffTime(defaultAntennaOffTime);

        setPower(defaultPower);

        setBLF(defaultBLF);

        setTARI(defaultTARI);

        setSession(defaultSession);

        setEncoding(defaultEncoding);

        setQValue(defaultQ);

        setTarget(defaultTarget);

        setFastSearch(defaultFastSearch);
    }

    public void loadConf() {

        properties = appContext.getApplicationContext().getSharedPreferences(Custom_Conf, Context.MODE_PRIVATE);

        //If there is no settings, use the default
        if (!properties.contains(Key_Operation_Mode)) {
            setDefault();
            return;
        }

        if (!properties.contains(Key_Reader_Timeout))
            setReaderTimeout(defaultReaderTimeout);

        if (!properties.contains(Key_Antenna_On_Time))
            setAntennaOnTime(defaultAntennaOnTime);

        if (!properties.contains(Key_Antenna_Off_Time))
            setAntennaOffTime(defaultAntennaOffTime);

        if (!properties.contains(Key_Power))
            setPower(defaultPower);

        if (!properties.contains(Key_BLF))
            setBLF(defaultBLF);

        if (!properties.contains(Key_TARI))
            setTARI(defaultTARI);

        if (!properties.contains(Key_Session))
            setSession(defaultSession);

        if (!properties.contains(Key_Encoding))
            setEncoding(defaultEncoding);

        if (!properties.contains(Key_Q_Value))
            setQValue(defaultQ);

        if (!properties.contains(Key_Target))
            setTarget(defaultTarget);

        if (!properties.contains(Key_Fast_Search))
            setFastSearch(defaultFastSearch);
    }

    public void setOperationMode(int mode) {
        properties.edit().putInt(Key_Operation_Mode, mode).commit();
    }

    public int getOperationMode() {
        if (!properties.contains(Key_Operation_Mode))
            setOperationMode(defaultMode);

        return properties.getInt(Key_Operation_Mode, defaultMode);
    }

    //region Reader Timeout
    public void setReaderTimeout(int timeout) {
        properties.edit().putInt(Key_Reader_Timeout, timeout).commit();
    }

    /**
     * Get the Reader Timeout value
     *
     * @return The value under current Operation Mode
     */
    public int getReaderTimeout() {
        return getReaderTimeout(getOperationMode());
    }

    /**
     * Get the Reader Timeout value
     *
     * @param mode Under which Operation Mode to read
     * @return Return the value under specific Operation Mode
     */
    public int getReaderTimeout(int mode) {
        if (!properties.contains(Key_Reader_Timeout))
            setReaderTimeout(defaultReaderTimeout);

        if (mode != Custom_Mode)
            return defaultReaderTimeout;

        return properties.getInt(Key_Reader_Timeout, defaultReaderTimeout);
    }
    //endregion

    //region Antenna On Time
    public void setAntennaOnTime(int onTime) {
        properties.edit().putInt(Key_Antenna_On_Time, onTime).commit();
    }

    public int getAntennaOnTime() {
        return getAntennaOnTime(getOperationMode());
    }

    public int getAntennaOnTime(int mode) {
        if (!properties.contains(Key_Antenna_On_Time))
            setAntennaOnTime(defaultAntennaOnTime);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 1000;
            case Rapid_Read_Multi_Mode:
                return 600;
            case Cycle_Count_Mode:
                return 300;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Antenna_On_Time, defaultAntennaOnTime);
    }
    //endregion

    //region Antenna Off Time
    public void setAntennaOffTime(int offTime) {
        properties.edit().putInt(Key_Antenna_Off_Time, offTime).commit();
    }

    public int getAntennaOffTime() {
        return getAntennaOffTime(getOperationMode());
    }

    public int getAntennaOffTime(int mode) {
        if (!properties.contains(Key_Antenna_Off_Time))
            setAntennaOffTime(defaultAntennaOffTime);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 0;
            case Rapid_Read_Multi_Mode:
                return 0;
            case Cycle_Count_Mode:
                return 300;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Antenna_Off_Time, defaultAntennaOffTime);
    }
    //endregion

    //region Power
    public void setPower(int power) {
        properties.edit().putInt(Key_Power, power).commit();
    }

    public int getPower() {
        return getPower(getOperationMode());
    }

    public int getPower(int mode) {
        if (!properties.contains(Key_Power))
            setPower(defaultPower);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 16;
            case Rapid_Read_Multi_Mode:
                return 16;
            case Cycle_Count_Mode:
                return 20;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Power, defaultPower);
    }
    //endregion

    //region Session
    public void setSession(int session) {
        properties.edit().putInt(Key_Session, session).commit();
    }

    public int getSession() {
        return getSession(getOperationMode());
    }

    public int getSession(int mode) {
        if (!properties.contains(Key_Session))
            setSession(defaultSession);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 0;
            case Rapid_Read_Multi_Mode:
                return 0;
            case Cycle_Count_Mode:
                return 1;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Session, defaultSession);
    }
    //endregion

    //region Target
    public void setTarget(int target) {
        properties.edit().putInt(Key_Target, target).commit();
    }

    public int getTarget() {
        return getTarget(getOperationMode());
    }

    public int getTarget(int mode) {
        if (!properties.contains(Key_Target))
            setTarget(defaultTarget);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 0;
            case Rapid_Read_Multi_Mode:
                return 2;
            case Cycle_Count_Mode:
                return 2;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Target, defaultTarget);
    }
    //endregion

    //region BLF
    public void setBLF(int blf) {
        properties.edit().putInt(Key_BLF, blf).commit();
    }

    public int getBLF() {
        return getBLF(getOperationMode());
    }

    public int getBLF(int mode) {
        if (!properties.contains(Key_BLF))
            setBLF(defaultBLF);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 1;
            case Rapid_Read_Multi_Mode:
                return 1;
            case Cycle_Count_Mode:
                return 0;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_BLF, defaultBLF);
    }
    //endregion BLF

    //region TARI
    public void setTARI(int tari) {
        properties.edit().putInt(Key_TARI, tari).commit();
    }

    public int getTARI() {
        return getTARI(getOperationMode());
    }

    public int getTARI(int mode) {
        if (!properties.contains(Key_TARI))
            setTARI(defaultTARI);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 2;
            case Rapid_Read_Multi_Mode:
                return 2;
            case Cycle_Count_Mode:
                return 0;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_TARI, defaultTARI);
    }
    //endregion

    //region Encoding
    public void setEncoding(int encoding) {
        properties.edit().putInt(Key_Encoding, encoding).commit();
    }

    public int getEncoding() {
        return getEncoding(getOperationMode());
    }

    public int getEncoding(int mode) {
        if (!properties.contains(Key_Encoding))
            setEncoding(defaultEncoding);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 0;
            case Rapid_Read_Multi_Mode:
                return 0;
            case Cycle_Count_Mode:
                return 3;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Encoding, defaultEncoding);
    }
    //endregion

    //region Q
    public void setQValue(int q) {
        properties.edit().putInt(Key_Q_Value, q).commit();
    }

    public int getQValue() {
        return getQValue(getOperationMode());
    }

    public int getQValue(int mode) {
        if (!properties.contains(Key_Q_Value))
            setQValue(defaultQ);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return 1;
            case Rapid_Read_Multi_Mode:
                return 0;
            case Cycle_Count_Mode:
                return 0;
            case Custom_Mode:
                break;
        }

        return properties.getInt(Key_Q_Value, defaultQ);
    }
    //endregion

    //region Fast Search
    public void setFastSearch(boolean fastSearch) {
        properties.edit().putBoolean(Key_Fast_Search, fastSearch).commit();
    }

    public boolean getFastSearch() {
        return getFastSearch(getOperationMode());
    }

    public boolean getFastSearch(int mode) {
        if (!properties.contains(Key_Fast_Search))
            setFastSearch(defaultFastSearch);

        switch (mode)
        {
            case Rapid_Read_Single_Mode:
                return true;
            case Rapid_Read_Multi_Mode:
                return false;
            case Cycle_Count_Mode:
                return false;
            case Custom_Mode:
                break;
        }

        return properties.getBoolean(Key_Fast_Search, defaultFastSearch);
    }
    //endregion
}

