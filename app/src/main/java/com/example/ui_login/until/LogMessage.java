package com.example.ui_login.until;

import android.util.Log;

import java.util.Date;

public class LogMessage {
    private static final String ERROR = "E";
    private static final String WARN = "W";
    private static final String INFO = "I";
    private static final String DEBUG = "D";
    private static final String VERBOSE = "V";

    public int LogLevel;
    public String Tag;
    public String Msg;
    public Date Time;

    public LogMessage(int logLevel, String tag, String msg, Date time) {
        LogLevel = logLevel;
        Tag = tag;
        Msg = msg;
        Time = time;
    }

    public String getLevelString() {
        switch (LogLevel) {
            default:
            case Log.ERROR:
                return ERROR;
            case Log.WARN:
                return WARN;
            case Log.INFO:
                return INFO;
            case Log.DEBUG:
                return DEBUG;
            case Log.VERBOSE:
                return VERBOSE;
        }
    }
}