package com.example.ui_login.until;

public class TimeoutCtrl {
    private long _startTime = 0;
    private long _timeout = 1000;

    /**
     * Initialize the class and setup the start time.
     * @param timeout The timeout value
     */
    public TimeoutCtrl(long timeout) {
        _startTime = System.currentTimeMillis();
        _timeout = timeout;
    }

    /**
     * Return is timeout or not
     * @return True for timeout, false for not
     */
    public boolean isTimeout() {
        return System.currentTimeMillis() - _startTime > _timeout;
    }

    /**
     * Reset the start time
     */
    public void reset() {
        _startTime = System.currentTimeMillis();
    }

    /**
     * Set the timeout value
     * @param timeout The timeout value
     */
    public void setTimeout(long timeout) {
        _timeout = timeout;
    }
}
