package com.example.ui_login.until;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.provider.DocumentsContract;

import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by USER
 */
public class DocumentFileUtils {
    private static final String LOG_TAG = DocumentFileUtils.class.getName();
    public static final String FILETYPE_PLAIN = "text/plain";

    private Context mContext;

    public DocumentFileUtils(Context context){
        mContext = context;
    }

    public DocumentFile getDirectory(Uri treeUri){
        DocumentFile pickedDir = DocumentFile.fromTreeUri(mContext, treeUri);
        return pickedDir;
    }

    @SuppressLint("NewApi")
    public DocumentFile getDocumentFileByPath(String path){
        DocumentFile file = null;

        Uri uri = Uri.fromFile(new File(path));

        mContext.getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        file = DocumentFile.fromFile(new File(path));

        return file;
    }

    public OutputStream getOutputStreamFromDocumentFile(DocumentFile file){
        OutputStream out = null;

        if (file != null){// && file.isFile()) {
            try {
                out = mContext.getContentResolver().openOutputStream(file.getUri());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return out;
    }

    public OutputStream getOutputStreamFromUri(Uri uri){
        OutputStream out = null;

        if (uri != null){// && file.isFile()) {
            try {
                out = mContext.getContentResolver().openOutputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return out;
    }

    public InputStream getInputStreamFromDocumentFile(DocumentFile file){
        InputStream in = null;

        if (file != null && file.isFile()) {
            try {
                in = mContext.getContentResolver().openInputStream(file.getUri());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return in;
    }

    public InputStream getInputStreamFromUri(Uri uri){
        InputStream in = null;

        if (uri != null) {
            try {
                in = mContext.getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return in;
    }

    public boolean createFile(DocumentFile folder, String filetype, String filename){
        DocumentFile newFile = folder.createFile(filetype, filename);
        if(newFile != null && newFile.exists() && newFile.isFile()){
            return true;
        } else {
            return false;
        }
    }

//    public boolean deleteFile(DocumentFile file){
//        return file.delete();
//    }

    private static final String PRIMARY_VOLUME_NAME = "primary";

    public static boolean isKitkat() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT;
    }
    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static String getSdCardPath() {
        String sdCardDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();

        try {
            sdCardDirectory = new File(sdCardDirectory).getCanonicalPath();
        }
        catch (IOException ioe) {
            LoggerUtil.error(LOG_TAG, "Could not get SD directory", ioe);
        }
        return sdCardDirectory;
    }

    @SuppressLint("NewApi")
    public static ArrayList<String> getExtSdCardPaths(Context con) {
        ArrayList<String> paths = new ArrayList<String>();
         File[] files = con.getExternalFilesDirs("external");
        File firstFile = files[0];
        for (File file : files) {
            if (file != null && !file.equals(firstFile)) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                    LoggerUtil.warn("", "Unexpected external file dir: " + file.getAbsolutePath());
                }
                else {
                    String path = file.getAbsolutePath().substring(0, index);
                    try {
                        path = new File(path).getCanonicalPath();
                    }
                    catch (IOException e) {
                        // Keep non-canonical path.
                    }
                    paths.add(path);
                }
            }
        }
        return paths;
    }

    public static String getFullPathFromTreeUri(@Nullable final Uri treeUri, Context con) {
        if (treeUri == null) {
            return null;
        }
        String volumePath = getVolumePath(getVolumeIdFromTreeUri(treeUri),con);
        if (volumePath == null) {
            return File.separator;
        }
        if (volumePath.endsWith(File.separator)) {
            volumePath = volumePath.substring(0, volumePath.length() - 1);
        }

        String documentPath = getDocumentPathFromTreeUri(treeUri);
        if (documentPath.endsWith(File.separator)) {
            documentPath = documentPath.substring(0, documentPath.length() - 1);
        }

        if (documentPath.length() > 0) {
            if (documentPath.startsWith(File.separator)) {
                return volumePath + documentPath;
            }
            else {
                return volumePath + File.separator + documentPath;
            }
        }
        else {
            return volumePath;
        }
    }

    public static String getFullPathFromUri(@Nullable final Uri uri, Context con) {
        if (uri == null) {
            return null;
        }
        String volumePath = getVolumePath(getVolumeIdFromUri(uri),con);
        if (volumePath == null) {
//            return File.separator;
            volumePath = "";
        }
        if (volumePath.endsWith(File.separator)) {
            volumePath = volumePath.substring(0, volumePath.length() - 1);
        }

        String documentPath = getDocumentPathFromUri(uri);
        if (documentPath.endsWith(File.separator)) {
            documentPath = documentPath.substring(0, documentPath.length() - 1);
        }

        if (documentPath.length() > 0) {
            if (documentPath.startsWith(File.separator)) {
                return volumePath + documentPath;
            }
            else {
                return volumePath + File.separator + documentPath;
            }
        }
        else {
            return volumePath;
        }
    }

    private static String getVolumePath(final String volumeId, Context con) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return null;
        }

        try {
            StorageManager mStorageManager =
                    (StorageManager) con.getSystemService(Context.STORAGE_SERVICE);

            Class<?> storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");

            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getUuid = storageVolumeClazz.getMethod("getUuid");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isPrimary = storageVolumeClazz.getMethod("isPrimary");
            Object result = getVolumeList.invoke(mStorageManager);

            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String uuid = (String) getUuid.invoke(storageVolumeElement);
                Boolean primary = (Boolean) isPrimary.invoke(storageVolumeElement);

                // primary volume?
                if (primary && PRIMARY_VOLUME_NAME.equals(volumeId)) {
                    return (String) getPath.invoke(storageVolumeElement);
                }

                // other volumes?
                if (uuid != null) {
                    if (uuid.equals(volumeId)) {
                        return (String) getPath.invoke(storageVolumeElement);
                    }
                }
            }

            // not found.
            return null;
        }
        catch (Exception ex) {
            return null;
        }
    }

    private static String getVolumeIdFromTreeUri(final Uri treeUri) {
        final String docId = DocumentsContract.getTreeDocumentId(treeUri);
        final String[] split = docId.split(":");

        if (split.length > 0) {
            return split[0];
        }
        else {
            return null;
        }
    }

    private static String getVolumeIdFromUri(final Uri uri) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");

        if (split.length > 0) {
            return split[0];
        }
        else {
            return null;
        }
    }

    private static String getDocumentPathFromTreeUri(final Uri treeUri) {
        final String docId = DocumentsContract.getTreeDocumentId(treeUri);
        final String[] split = docId.split(":");
        if ((split.length >= 2) && (split[1] != null)) {
            return split[1];
        }
        else {
            return File.separator;
        }
    }

    private static String getDocumentPathFromUri(final Uri uri) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        if ((split.length >= 2) && (split[1] != null)) {
            return split[1];
        }
        else {
            return File.separator;
        }
    }
}
