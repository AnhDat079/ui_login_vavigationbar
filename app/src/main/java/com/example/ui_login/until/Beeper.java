package com.example.ui_login.until;

import android.media.AudioManager;
import android.media.ToneGenerator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by USER
 */
public class Beeper {

    private static Beeper mInstance;

    private final ToneGenerator toneGen;
    private final ExecutorService executorService;
    private ReentrantLock beeperLock, beepingLock;
    private boolean canBeep = false, toBeep = false;

    public static Beeper getInstance() {
        if (mInstance == null) {
            mInstance = new Beeper();
        }

        return mInstance;
    }

    private Beeper() {
        this.toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        this.beeperLock = new ReentrantLock();
        this.beepingLock = new ReentrantLock();
        this.executorService = Executors.newSingleThreadExecutor();
    }

    public void setCanBeep(boolean canBeep) {
        this.canBeep = canBeep;
    }

    public void beep() {
        if (canBeep && !toBeep && beeperLock.tryLock()) {
            toBeep = true;
            executorService.submit(runBeep);
            beeperLock.unlock();
        }
    }

    //Jack
    public void beepLocate(final int grid, final int gridSize  ) {
        if (canBeep && !toBeep && beeperLock.tryLock()) {
            toBeep = true;
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    if (beepingLock.tryLock() && toBeep && canBeep) {
                        float percentage = (float) grid / gridSize;
                        LoggerUtil.debug("beepLocate", "percentage = " + percentage);
                        int beeptone = ToneGenerator.TONE_CDMA_LOW_L;
                        int duration = 25;
                        if (percentage >= 0.9) {
                            beeptone = ToneGenerator.TONE_CDMA_HIGH_L;//3700
                        } else if (percentage >= 0.8 && percentage < 0.9) {
                            beeptone = ToneGenerator.TONE_CDMA_HIGH_L;//3700
                        } else if (percentage >= 0.7 && percentage < 0.8) {
                            beeptone = ToneGenerator.TONE_CDMA_MED_L;//2600
                        } else if (percentage >= 0.6 && percentage < 0.7) {
                            beeptone = ToneGenerator.TONE_CDMA_MED_L;//2600
                        } else if (percentage >= 0.5 && percentage < 0.6) {
                            beeptone = ToneGenerator.TONE_CDMA_LOW_L;//1300
                        } else if (percentage >= 0.4 && percentage < 0.5) {
                            beeptone = ToneGenerator.TONE_CDMA_LOW_L;//1300
                        } else if (percentage >= 0.3 && percentage < 0.4) {
                            beeptone = ToneGenerator.TONE_CDMA_ABBR_ALERT;//1150
//                            duration = 100;
                        } else if (percentage >= 0.2 && percentage < 0.3) {
                            beeptone = ToneGenerator.TONE_CDMA_ABBR_ALERT;//1150
//                            duration = 100;
                        } else if (percentage >= 0.1 && percentage < 0.2) {
                            beeptone = ToneGenerator.TONE_CDMA_PRESSHOLDKEY_LITE;//587
//                            duration = 375;
                        } else if (percentage < 0.1) {
                            beeptone = ToneGenerator.TONE_CDMA_PRESSHOLDKEY_LITE;//587
//                            duration = 375;
                        }

                        toneGen.startTone(beeptone, duration);
                        try {
                            Thread.sleep(duration * 2);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        toBeep = false;
                        beepingLock.unlock();
                    }
                }
            });
            beeperLock.unlock();
        }
    }

    public void tryBeep() {
        beepingLock.lock();
        toneGen.startTone(ToneGenerator.TONE_CDMA_LOW_L, 25);
        beepingLock.unlock();
    }

    public void stopAllBeep() {
        beeperLock.lock();
        toBeep = false;
        beeperLock.unlock();
    }

    private Runnable runBeep = new Runnable() {
        @Override
        public void run() {
            if (beepingLock.tryLock() && toBeep && canBeep) {
                toneGen.startTone(ToneGenerator.TONE_CDMA_LOW_L, 25);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toBeep = false;
                beepingLock.unlock();
            }
        }
    };
}
