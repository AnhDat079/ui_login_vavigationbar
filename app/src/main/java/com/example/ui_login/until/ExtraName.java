package com.example.ui_login.until;

public interface ExtraName {
    String LOCATE_EPC = "LOCATE_EPC";
    String LOCATE_RSSI = "LOCATE_RSSI";
    String Start = "start";
    String Enable = "enable";
    String Gun_State = "EXTRA_EXTENDED_STATE";
}
