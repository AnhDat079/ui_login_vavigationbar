package com.example.ui_login.until;

public interface ActionName {
    //For Barcode
    String BARCODE_DATA_ACTION = "ui_login.scanservice.data";
    String BARCODE_DATA_LENGTH_ACTION = "ui_login.scanservice.databytelength";

    //For local broadcast
    String ACTION_LOCATE_DATA = "ui_login.locate.data";
    String ACTION_INITIAL = "ui_login.initial";
    String ACTION_LOCATE_READING = "com.example.ui_login.locate.reading";

    //For select EPC
    String Action_SelectEPC = "com.example.ui_login.selectEPC";

    String GUN_DETECT = "com.example.ui_login.EXTENDED_PORT";

}
