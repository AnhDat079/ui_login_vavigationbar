package com.example.ui_login.until;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoggerUtil{
        public static final String APP_ID = "RFIDReaderAPP";
        public static final String DIR_LOG = "RFIDReader";
        private static String logFileName = "/RFID-Debug.txt";
        private static boolean writeLogsToFile = true;
        private static final int LOG_LEVEL_VERBOSE = 2;
        private static final int LOG_LEVEL_DEBUG = 3;
        private static final int LOG_LEVEL_INFO = 4;
        private static final int LOG_LEVEL_WARN = 5;
        private static final int LOG_LEVEL_ERROR = 6;
        private static final int LOG_LEVEL_OFF = 8;
        private static final int CURRENT_LOG_LEVEL = LOG_LEVEL_DEBUG;

        private static final DateFormat dfFilename = new SimpleDateFormat("yyyyMMddHHmmss");
        private static final DateFormat dfLog = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

        private static ExecutorService executor = Executors.newSingleThreadExecutor();
        private static PrintWriter writer;
        private static ConcurrentLinkedQueue<LogMessage> messageQueue = new ConcurrentLinkedQueue<>();

        public static void log(String tag, String message, int logLevel) {
            if (logLevel >= CURRENT_LOG_LEVEL)
                if (writeLogsToFile) {
//                writeToFile(message);
                    writeToQueue(logLevel, tag, message, new Date());
                }
        }

        private static void writeToQueue(int logLevel, String tag, String message, Date time) {
            messageQueue.add(new LogMessage(logLevel, tag, message, time));
        }

        private static void writeToFile(String message) {
            try {
                if (writer != null) {
                    writer.println(message);
                }
            } catch (Exception e) {
                LoggerUtil.error(APP_ID, "Exception in logging  :", e);
            }
        }

        public static void verbose(String tag, String message) {
            Log.v(tag, message);
            log(tag, message, LOG_LEVEL_VERBOSE);
        }

        public static void debug(String tag, String message) {
            Log.d(tag, message);
            log(tag, message, LOG_LEVEL_DEBUG);
        }

        public static void error(String tag, String message, Exception ex) {
            Log.e(tag, message, ex);
            log(tag, message + ex.toString(), LOG_LEVEL_ERROR);
        }

        public static void error(String tag, String message) {
            Log.e(tag, message);
            log(tag, message, LOG_LEVEL_ERROR);
        }

        public static void warn(String tag, String message) {
            Log.w(tag, message);
            log(tag, message, LOG_LEVEL_WARN);
        }

        public static void warn(String tag, String message, Exception ex) {
            Log.w(tag, message, ex);
            log(tag, message + ex.toString(), LOG_LEVEL_WARN);
        }

        public static void info(String tag, String message) {
            Log.i(tag, message);
            log(tag, message, LOG_LEVEL_INFO);
        }

        private static final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                int count = 0;
                while (writeLogsToFile) {
                    while (!messageQueue.isEmpty()) {
                        LogMessage msg = messageQueue.poll();
                        writeToFile(String.format("%s %s/%s: %s", dfLog.format(msg.Time), msg.getLevelString(), msg.Tag, msg.Msg));
                        count++;
                        if (count > 20) {
                            count = 0;
                            writer.flush();
                        }
                    }
                    writer.flush();

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        public static void initialize(Context context) {
            String file = String.format("%s/log_%s.txt", context.getExternalFilesDir(null).getAbsolutePath(), dfFilename.format(new Date()));
            try {
                writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true), 8 * 1024));
            } catch (IOException e) {
                e.printStackTrace();
            }

            executor.submit(runnable);
        }
}
