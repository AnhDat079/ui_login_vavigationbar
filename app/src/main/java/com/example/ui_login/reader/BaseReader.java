package com.example.ui_login.reader;

import android.content.Context;

import com.example.ui_login.enums.ActionState;
import com.example.ui_login.enums.ConnectState;
import com.example.ui_login.enums.CommandType;
import com.example.ui_login.until.LoggerUtil;
import com.thingmagic.TagReadData;


public abstract class BaseReader {
    private static final String TAG = BaseReader.class.getSimpleName();
    protected Context _context;

    private ReaderEventManager _readerEventManager;

    private volatile ConnectState _connectState;
    protected volatile ActionState _actionState;

    BaseReader(Context context) {
        this._context = context;
        this._readerEventManager = new ReaderEventManager();
        this._connectState = ConnectState.Disconnected;
        this._actionState = ActionState.Stop;
    }

    public void destroy() {
        this._readerEventManager.destroy();
    }

    /**
     * Adds a Listener to receive events on an instance of the BaseReader class. Events are only
     * notified to the last added Listener.
     *
     * @param listener An instance of IReaderEventListener to receive events fired by instances of
     *                 the BaseReader class.
     */
    public void addListener(IReaderEventListener listener) {
        this._readerEventManager.addListener(listener);
    }

    /**
     * Removes the Listener that is listening for events on an instance of the BaseReader class.
     * If the Listener being deleted is the last event, the Listener that was added before the
     * last is notified of the event.
     *
     * @param listener An instance of IReaderEventListener that is receiving events fired by
     *                 instances of the BaseReader class.
     */
    public void removeListener(IReaderEventListener listener) {
        this._readerEventManager.removeListener(listener);
    }

    /**
     * Removes all listeners registered on an instance of the BaseReader class.
     */
    public void clearListener() {
        this._readerEventManager.clear();
    }

    public ConnectState getConnectState() {
        LoggerUtil.debug(TAG, "Connect State: " + _connectState.toString());
        return _connectState;
    }

    public ActionState getActionState() {
        return _actionState;
    }

    protected void onReaderConnectStateChanged(ConnectState state, Object params) {
        this._connectState = state;
        this._readerEventManager.generateReaderStateChanged(this, state, params);
    }

    public void onReaderActionChanged(ActionState action, Object params) {
        this._actionState = action;
        this._readerEventManager.generateReaderActionChanged(this, action, params);
    }

    public void onReaderReadTag(TagReadData tagReadData) {
        this._readerEventManager.generateReaderReadTag(this, tagReadData);
    }

    protected void onReaderErrorEvent(CommandType commandType, String msg) {
        this._readerEventManager.generateReaderErrorEvent(this, commandType, msg);
    }

    public void onReaderTemperatureChanged(int temperature) {
        this._readerEventManager.generateReaderTemperatureChangedEvent(this, temperature);
    }

    public abstract void connect(String region);

    public abstract void disconnect();

    public abstract void inventory();

    public abstract void stop();
}
