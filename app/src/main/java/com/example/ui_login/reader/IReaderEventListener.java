package com.example.ui_login.reader;

import com.example.ui_login.enums.ActionState;
import com.example.ui_login.enums.CommandType;
import com.example.ui_login.enums.ConnectState;
import com.thingmagic.TagReadData;

public interface IReaderEventListener {

    /**
     * Called when the operational state of the reader instance changes.
     *
     * @param reader      The changed reader
     * @param actionState The changed action
     * @param params      The parameter
     */
    void onReaderActionChanged(BaseReader reader,  ActionState actionState, Object params);

    /**
     * Called when the connection state of the reader changes.
     *
     * @param reader       The BaseReader instance that fired the event.
     * @param connectState A ConnectState enumeration indicating the connection status with the Device.
     * @param params       If the event is accompanied by additional information, a non-null value is passed to the instance.
     */
    void onReaderConnectStateChanged(BaseReader reader, ConnectState connectState, Object params);

    /**
     * Called when read tag from RFID module
     *
     * @param reader      The BaseReader instance that fired the event.
     * @param tagReadData The tag data which read from RFID module
     */
    void onReaderReadTag(BaseReader reader, TagReadData tagReadData);

    /**
     * Called when error occur
     *
     * @param reader      The BaseReader instance that fired the event.
     * @param commandType The command type about the error.
     * @param msg         The error message.
     */
    void onReaderErrorEvent(BaseReader reader, CommandType commandType, String msg);

    /**
     * Called when temperature changed
     *
     * @param reader      The BaseReader instance that fired the event.
     * @param temperature The temperature value.
     */
    void onReaderTemperatureChanged(BaseReader reader, int temperature);
}