package com.example.ui_login.reader;

import android.os.Handler;
import android.os.Looper;

import com.example.ui_login.enums.ActionState;
import com.example.ui_login.enums.CommandType;
import com.example.ui_login.enums.ConnectState;
import com.example.ui_login.until.LoggerUtil;
import com.thingmagic.TagReadData;

import java.util.ArrayList;
import java.util.Locale;

public class ReaderEventManager {
    private static final int EVENT_CONNECT_STATE_CHANGED = 1000;
    private static final int EVENT_ACTION_CHANGED = 1001;
    private static final int EVENT_READ_TAG = 1002;
    private static final int EVENT_ERROR_EVENT = 1003;
    private static final int EVENT_TEMPERATURE_CHANGED = 1004;

    private static final String TAG = ReaderEventManager.class.getSimpleName();
    private Handler.Callback mEventCallback = msg -> {
        switch (msg.what) {
            case EVENT_CONNECT_STATE_CHANGED:
                ReaderEventManager.this.onConnectStateChanged((ConnectStateChangedEventArgs) msg.obj);
                return true;
            case EVENT_ACTION_CHANGED:
                ReaderEventManager.this.onActionChanged((ActionChangedEventArgs) msg.obj);
                return true;
            case EVENT_READ_TAG:
                ReaderEventManager.this.onReadTag((ReaderReadTagEventArgs) msg.obj);
                return true;
            case EVENT_ERROR_EVENT:
                ReaderEventManager.this.onErrorEvent((ReaderErrorEventArgs) msg.obj);
                return true;
            case EVENT_TEMPERATURE_CHANGED:
                ReaderEventManager.this.onTemperatureChanged((ReaderTemperatureChangedEventArgs) msg.obj);
                return true;
            default:
                return false;
        }
    };
    private Handler mHandler = new Handler(Looper.getMainLooper(), this.mEventCallback);
    private ArrayList<IReaderEventListener> mListeners = new ArrayList<>();

    public void destroy() {
        if (this.mListeners != null && this.mListeners != null) {
            this.mListeners.clear();
            this.mListeners = null;
        }
    }

    public void addListener(IReaderEventListener listener) {
        if (this.mListeners != null) {
            this.mListeners.add(listener);
        }
    }

    public void removeListener(IReaderEventListener listener) {
        if (this.mListeners != null) {
            this.mListeners.remove(listener);
        }
    }

    public void clear() {
        if (this.mListeners != null) {
            this.mListeners.clear();
        }
    }

    public void generateReaderStateChanged(BaseReader reader, ConnectState state, Object params) {
        if (this.mListeners != null && !this.mListeners.isEmpty()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(EVENT_CONNECT_STATE_CHANGED, new ConnectStateChangedEventArgs(reader, state, params)));
        }
    }

    public void generateReaderActionChanged(BaseReader reader, ActionState action, Object params) {
        if (this.mListeners != null && !this.mListeners.isEmpty()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(EVENT_ACTION_CHANGED, new ActionChangedEventArgs(reader, action, params)));
        }
    }

    public void generateReaderReadTag(BaseReader reader, TagReadData tagReadData) {
        if (this.mListeners != null && !this.mListeners.isEmpty()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(EVENT_READ_TAG, new ReaderReadTagEventArgs(reader, tagReadData)));
        }
    }

    public void generateReaderErrorEvent(BaseReader reader, CommandType commandType, String msg) {
        if (this.mListeners != null && !this.mListeners.isEmpty()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(EVENT_ERROR_EVENT, new ReaderErrorEventArgs(reader, commandType, msg)));
        }
    }

    public void generateReaderTemperatureChangedEvent(BaseReader reader, int temperature) {
        if (this.mListeners != null && !this.mListeners.isEmpty()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(EVENT_TEMPERATURE_CHANGED, new ReaderTemperatureChangedEventArgs(reader, temperature)));
        }
    }

    private IReaderEventListener getLastListener() {
        if (this.mListeners == null) {
            return null;
        } else {
            int index = this.mListeners.size() - 1;
            return index < 0 ? null : (IReaderEventListener) this.mListeners.get(index);
        }
    }

    private void onConnectStateChanged(ConnectStateChangedEventArgs args) {
        IReaderEventListener listener = getLastListener();
        if (listener != null) {
            try {
                listener.onReaderConnectStateChanged(args.Reader, args.State, args.Params);
            } catch (Exception e) {
                LoggerUtil.error(TAG, String.format("onConnectStateChanged([%s]: %s)", args.toString(), e));
            }
        }
    }

    private void onActionChanged(ActionChangedEventArgs args) {
        IReaderEventListener listener = getLastListener();
        if (listener != null) {
            try {
                listener.onReaderActionChanged(args.Reader, args.Action, args.Params);
            } catch (Exception e) {
                LoggerUtil.error(TAG, String.format("onActionChanged([%s]: %s)", args.toString(), e));
            }
        }
    }

    private void onReadTag(ReaderReadTagEventArgs args) {
        IReaderEventListener listener = getLastListener();
        if (listener != null) {
            try {
                listener.onReaderReadTag(args.Reader, args.tagReadData);
            } catch (Exception e) {
                LoggerUtil.error(TAG, String.format("onReadTag([%s]: %s)", args.toString(), e));
            }
        }
    }

    private void onErrorEvent(ReaderErrorEventArgs args) {
        IReaderEventListener listener = getLastListener();
        if (listener != null) {
            try {
                listener.onReaderErrorEvent(args.Reader, args.commandType, args.msg);
            } catch (Exception e) {
                LoggerUtil.error(TAG, String.format("onErrorEvent([%s]: %s)", args.toString(), e));
            }
        }
    }

    private void onTemperatureChanged(ReaderTemperatureChangedEventArgs args) {
        IReaderEventListener listener = getLastListener();
        if (listener != null) {
            try {
                listener.onReaderTemperatureChanged(args.Reader, args.Temperature);
            } catch (Exception e) {
                LoggerUtil.error(TAG, String.format("onTemperatureChanged([%s]: %s)", args.toString(), e));
            }
        }
    }

    public static class ConnectStateChangedEventArgs {
        private final Object Params;
        private final BaseReader Reader;
        private final ConnectState State;

        protected ConnectStateChangedEventArgs(BaseReader reader, ConnectState state, Object params) {
            this.Reader = reader;
            this.State = state;
            this.Params = params;
        }

        public String toString() {
            return String.format(Locale.US, "[%s], %s", this.Reader, this.State);
        }
    }

    public static class ActionChangedEventArgs {
        private final ActionState Action;
        private final Object Params;
        private final BaseReader Reader;

        protected ActionChangedEventArgs(BaseReader reader, ActionState action, Object params) {
            this.Reader = reader;
            this.Action = action;
            this.Params = params;
        }

        public String toString() {
            return String.format(Locale.US, "[%s], %s", this.Reader, this.Action);
        }
    }

    public static class ReaderReadTagEventArgs {
        private final TagReadData tagReadData;
        private final BaseReader Reader;

        protected ReaderReadTagEventArgs(BaseReader reader, TagReadData tagReadData) {
            this.Reader = reader;
            this.tagReadData = tagReadData;
        }

        public String toString() {
            return String.format(Locale.US, "[%s], %s", this.Reader, this.tagReadData);
        }
    }

    public static class ReaderErrorEventArgs {
        private final BaseReader Reader;
        private final String msg;
        private final CommandType commandType;

        protected ReaderErrorEventArgs(BaseReader reader, CommandType commandType, String msg) {
            this.Reader = reader;
            this.commandType = commandType;
            this.msg = msg;
        }

        public String toString() {
            return String.format(Locale.US, "[%s] %s", commandType.toString(), msg);
        }
    }

    public static class ReaderTemperatureChangedEventArgs {
        private final BaseReader Reader;
        private final int Temperature;

        protected ReaderTemperatureChangedEventArgs(BaseReader reader, int temperature) {
            this.Reader = reader;
            this.Temperature = temperature;
        }

        public String toString() {
            return String.format(Locale.US, "[%d]", Temperature);
        }
    }
}
