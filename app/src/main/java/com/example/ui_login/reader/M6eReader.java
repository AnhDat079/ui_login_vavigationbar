package com.example.ui_login.reader;

import static com.example.ui_login.until.Utilities.BUNDLE_ERROR_CODE;
import static com.example.ui_login.until.Utilities.BUNDLE_ERROR_MSG;
import static com.example.ui_login.until.Utilities.DEFAULT_TIMEOUT;
import static com.example.ui_login.until.Utilities.RESULT_CODE_ERROR;
import static com.example.ui_login.until.Utilities.RESULT_CODE_SUCCESS;
import static com.example.ui_login.until.Utilities.setBundleResult;
import static com.example.ui_login.until.Utilities.sleep;

import android.content.Context;
import android.os.Bundle;

import com.example.ui_login.enums.ActionState;
import com.example.ui_login.enums.CommandType;
import com.example.ui_login.enums.ConnectState;
import com.example.ui_login.enums.GunState;
import com.example.ui_login.until.LoggerUtil;
import com.example.ui_login.until.TimeoutCtrl;
import com.thingmagic.ReadExceptionListener;
import com.thingmagic.ReadListener;
import com.thingmagic.Reader;
import com.thingmagic.ReaderException;
import com.thingmagic.SerialTransportRS232;
import com.thingmagic.StatusListener;
import com.thingmagic.TMConstants;
import com.thingmagic.TagFilter;
import com.thingmagic.TagOp;
import com.unitech.api.pogo.PogoCtrl;
import com.unitech.util.Machine;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class M6eReader extends BaseReader{private static final String TAG = M6eReader.class.getSimpleName();
    private String _uartPath;
    private ExecutorService executorGetUART;
    private boolean _getUARTFinished;
    private boolean _getUARTResult;

    private ExecutorService executorConnect;
    private Future<?> futureConnect;

    private ExecutorService executorAction;
    private Future<?> futureAction;

    private Reader _reader;
    private Reader.Region _region;

    private boolean _targetConnectState;
    private GunState _gunState;

    /**
     * Used to cancel the connection thread
     */
    private boolean _cancel;

    public M6eReader(Context context) {
        super(context);

        _reader = null;
        _cancel = false;
        _targetConnectState = false;
        _getUARTResult = false;
        _gunState = GunState.Unknown;

        executorGetUART = Executors.newSingleThreadExecutor();
        executorConnect = Executors.newSingleThreadExecutor();
        executorAction = Executors.newSingleThreadExecutor();

        getUARTPath();
    }

    @Override
    public void connect(String region) {
        LoggerUtil.debug(TAG, "connect()");

        _region = Reader.Region.valueOf(region);

        connectCtrl(true);
    }

    @Override
    public void disconnect() {
        LoggerUtil.debug(TAG, "disconnect()");
        connectCtrl(false);
    }

    @Override
    public void inventory() {
        try {
            assertReader();
        } catch (Exception e) {
            LoggerUtil.error(TAG, e.getMessage());
            return;
        }

        if (getActionState() == ActionState.Inventory) {
            LoggerUtil.debug(TAG, "Already inventory");
            return;
        }

        if (futureAction != null) {
            if (!(futureAction.isDone() || futureAction.isCancelled())) {
                LoggerUtil.error(TAG, "The executorAction does not finish.");
                return;
            }
        }

        if (executorAction == null || executorAction.isShutdown()) {
            executorAction = Executors.newSingleThreadExecutor();
        }

        futureAction = executorAction.submit(() -> {
            LoggerUtil.debug(TAG, "+++ Inventory executor()");
            onReaderActionChanged(ActionState.Inventorying, null);
            _reader.startReading();
            onReaderActionChanged(ActionState.Inventory, null);
            LoggerUtil.debug(TAG, "--- Inventory executor()");
        });
    }

    @Override
    public void stop() {
        try {
            assertReader();
        } catch (Exception e) {
            LoggerUtil.error(TAG, e.getMessage());
            return;
        }

        if (getActionState() == ActionState.Stop) {
            LoggerUtil.debug(TAG, "Already stop");
            return;
        }

        if (futureAction != null) {
            if (!(futureAction.isDone() || futureAction.isCancelled())) {
                LoggerUtil.error(TAG, "The executorAction does not finish.");
                return;
            }
        }

        if (executorAction == null || executorAction.isShutdown()) {
            executorAction = Executors.newSingleThreadExecutor();
        }

        futureAction = executorAction.submit(() -> {
            LoggerUtil.debug(TAG, "+++ Stop executor()");
            onReaderActionChanged(ActionState.Stopping, null);
            boolean result = _reader.stopReading();
            onReaderActionChanged(ActionState.Stop, null);
            if (!result) {
                LoggerUtil.error(TAG, "Stop reading failed");
                disconnect();
            }
            LoggerUtil.debug(TAG, "--- Stop executor()");
        });
    }

    @Override
    public void destroy() {
        super.destroy();
        if (executorGetUART != null) {
            executorGetUART.shutdown();
        }

        if (executorConnect != null) {
            executorConnect.shutdown();
        }

        if (executorAction != null) {
            executorAction.shutdown();
        }
    }

    /**
     * To connect/disconnect with reader
     *
     * @param enable True for connect, false for disconnect
     */
    private void connectCtrl(boolean enable) {
        TimeoutCtrl tc = new TimeoutCtrl(DEFAULT_TIMEOUT);

        if (!_getUARTResult) {
            //Get the UART, because of the result is false and thread is finished
            if (getGetUARTFinished()) {
                getUARTPath();
            }

            while (!getGetUARTFinished()) {
                LoggerUtil.error(TAG, "Waiting get UART path finished");
                sleep(10);

                if (tc.isTimeout()) {
                    LoggerUtil.error(TAG, "Waiting get UART path timeout");
                    onReaderErrorEvent(CommandType.UART, "Get UART timeout");
                    return;
                }
            }

            if (!_getUARTResult) {
                LoggerUtil.error(TAG, "Get UART fail");
                return;
            }
        }

        _targetConnectState = enable;
        LoggerUtil.debug(TAG, "Connect State should be " + _targetConnectState);

        if (futureConnect != null) {
            if (!(futureConnect.isDone() || futureConnect.isCancelled())) {
                LoggerUtil.debug(TAG, "The executorConnect does not finish.");
//                onReaderErrorEvent(enable ? CommandType.Connect : CommandType.Disconnect, "Connect thread is busy.");
                return;
            }
        }

        try {
            resetCancel();

            ConnectThread connectThread = new ConnectThread(this);

            if (executorConnect == null || executorConnect.isShutdown()) {
                executorConnect = Executors.newSingleThreadExecutor();
            }

            futureConnect = executorConnect.submit(connectThread);
        } catch (Exception e) {
            LoggerUtil.error(TAG, "Create ConnectThread exception: " + e);
            onReaderErrorEvent(enable ? CommandType.Connect : CommandType.Disconnect, e.toString());
        }
    }

    private class ConnectThread implements Runnable {
        private final M6eReader _m6eReader;

        public ConnectThread(M6eReader reader) {
            this._m6eReader = reader;
        }

        @Override
        public void run() {
            LoggerUtil.info(TAG, "+++ ConnectThread()");
            boolean currentState = (getConnectState() == ConnectState.Connected);
            int retryCount = 3;

            while (currentState != _targetConnectState) {
                if (retryCount == 0) {
                    LoggerUtil.error(TAG, "Retry finished");
                    break;
                }

//                if (isCancel()) {
//                    LoggerUtil.error(TAG, "Canceled");
//                    break;
//                }
                resetCancel();

                switch (getGunState()) {
                    case Unknown:
                    case Attached:
                        if (_targetConnectState) {
                            doConnect();

                            if (getConnectState() != ConnectState.Connected) {
                                LoggerUtil.error(TAG, "Connect failed because of connect state is not connected");
                                if (Machine.EA630Plus) {
                                    retryCount--;
                                } else {
                                    retryCount = 0;
                                }
                            } else {
                                retryCount = 3;
                            }
                        } else {
                            doDisconnect();
                            retryCount = 3;
                        }
                        break;
                    case Unattached:
                        if (_targetConnectState) {
                            LoggerUtil.debug(TAG, "Gun is unattached, could not connect");
                            onReaderErrorEvent(CommandType.Connect, "Gun is unattached.");
                            retryCount = 0;
                        } else {
                            retryCount = 3;
                        }
                        doDisconnect();
                        break;
                }

                sleep(100);

                currentState = (getConnectState() == ConnectState.Connected);
            }

            LoggerUtil.info(TAG, "--- ConnectThread()");
        }
    }

    /**
     * Do connect to module and do not care the connect state
     */
    private void doConnect() {
        LoggerUtil.debug(TAG, "Do connect");

        onReaderConnectStateChanged(ConnectState.Connecting, null);

        //region Turn off then turn on RFID module's power
        PogoCtrl pogoCtrl;
        try {
            pogoCtrl = PogoCtrl.getInstance(_context);
        } catch (final Exception ex) {
            LoggerUtil.error(TAG, "Initial PogoCtrl exception: " + ex.getMessage());
            onReaderErrorEvent(CommandType.PowerOn, "Initial PogoCtrl exception.");
            onReaderConnectStateChanged(ConnectState.Disconnected, null);
            return;
        }

        pogoCtrl.powerOff();

        sleep(100);

        Bundle result = pogoCtrl.powerOn();

        if (result.getInt(BUNDLE_ERROR_CODE) == RESULT_CODE_ERROR) {
            LoggerUtil.error(TAG, "Power on fail: " + result.getString(BUNDLE_ERROR_MSG));
            onReaderErrorEvent(CommandType.PowerOn, result.getString(BUNDLE_ERROR_MSG));
            onReaderConnectStateChanged(ConnectState.Disconnected, null);
            return;
        }

        sleep(300);
        //endregion

        try {
            if (_reader == null) {
                Reader.setSerialTransport("ute", new SerialTransportRS232.Factory());
                _reader = Reader.create(_uartPath);
            }

            if (_reader == null) {
                LoggerUtil.error(TAG, "Reader.create() return null.");
                onReaderConnectStateChanged(ConnectState.Disconnected, null);
                return;
            }

            if (isCancel()) {
                canceled();
                return;
            }

            LoggerUtil.info(TAG, "reader: " + _reader);
            _reader.connect();

            if (isCancel()) {
                canceled();
                return;
            }

            LoggerUtil.debug(TAG, "Set Region: " + _region);
            _reader.paramSet(TMConstants.TMR_PARAM_REGION_ID, _region);

        } catch (Exception ex) {
            LoggerUtil.error(TAG, "Connect to reader get exception: " + ex);
            onReaderErrorEvent(CommandType.Connect, ex.toString());
            onReaderConnectStateChanged(ConnectState.Disconnected, null);
            return;
        }

        setGunState(GunState.Attached);
        onReaderConnectStateChanged(ConnectState.Connected, null);
    }

    /**
     * Do disconnect with module and do not care connect state
     */
    private void doDisconnect() {
        LoggerUtil.debug(TAG, "Do disconnect");

        onReaderConnectStateChanged(ConnectState.Disconnecting, null);

        stop();

        onReaderActionChanged(ActionState.Stop, null);

        //region Turn off RFID module's power
        try {
            PogoCtrl pogoCtrl = PogoCtrl.getInstance(_context);

            pogoCtrl.powerOff();
        } catch (final Exception ex) {
            LoggerUtil.error(TAG, "Power off exception : " + ex.getMessage());
            onReaderErrorEvent(CommandType.PowerOff, ex.toString());
        }
        //endregion

        if (_reader != null) {
            _reader.destroy();
        }

        if (executorAction != null) {
            executorAction.shutdown();
        }
        futureAction = null;

        onReaderConnectStateChanged(ConnectState.Disconnected, null);
    }

    //region Listener Control
    public void addReadListener(ReadListener listener) {
        if (_reader != null && listener != null) {
            _reader.addReadListener(listener);
        }
    }

    public void addReadExceptionListener(ReadExceptionListener listener) {
        if (_reader != null && listener != null) {
            _reader.addReadExceptionListener(listener);
        }
    }

    public void addStatusListener(StatusListener listener) {
        if (_reader != null && listener != null) {
            _reader.addStatusListener(listener);
        }
    }

    public void removeReadListener(ReadListener listener) {

        if (_reader != null && listener != null) {
            _reader.removeReadListener(listener);
        }
    }

    public void removeReadExceptionListener(ReadExceptionListener listener) {
        if (_reader != null && listener != null) {
            _reader.removeReadExceptionListener(listener);
        }
    }

    public void removeStatusListener(StatusListener listener) {
        if (_reader != null && listener != null) {
            _reader.removeStatusListener(listener);
        }
    }
    //endregion

    //region Get/Set reader parameter
    public Object paramGet(String key) {
        try {
            if (getActionState() != ActionState.Stop) {
                LoggerUtil.error(TAG, "Reader is busy");
                return null;
            }

            if (_reader == null) {
                LoggerUtil.error(TAG, "Reader is null");
                return null;
            }

            if (getConnectState() != ConnectState.Connected){
                LoggerUtil.error(TAG, "Reader is not connected");
                return null;
            }

            return _reader.paramGet(key);
        } catch (ReaderException e) {
            LoggerUtil.error(TAG, "Get parameter exception: " + e);
            return null;
        }
    }

    public void paramSet(String key, Object value) throws ReaderException {
        if (_reader == null) {
            LoggerUtil.error(TAG, "Reader is null");
            return;
        }

        if (getConnectState() != ConnectState.Connected) {
            LoggerUtil.error(TAG, "Reader is not connected");
            return;
        }

        _reader.paramSet(key, value);
    }

    /**
     * Set the parameter to reader and if the value is the same with module will skip to set again.
     * If not, it will set and check the value.
     *
     * @param key   The parameter's key path.
     * @param value The parameter's value.
     * @return The result bundle
     */
    public Bundle setParam(String key, Object value) {
        try {
            assertReader();
        } catch (Exception e) {
            return setBundleResult(RESULT_CODE_ERROR, e.getMessage());
        }

        if (getActionState() != ActionState.Stop) {
            LoggerUtil.error(TAG, "Reader is busy");
            return setBundleResult(RESULT_CODE_ERROR, "Reader is busy");
        }

        Bundle resultBundle = setBundleResult(RESULT_CODE_SUCCESS, "");

        //region Verify current value, if same then return
        try {
            Object currentValue = _reader.paramGet(key);

            if (currentValue.toString().equals(value.toString())) {
                LoggerUtil.debug(TAG, String.format("Parameter %s is the same with current value %s", key, currentValue));
                return resultBundle;
            } else {
                LoggerUtil.debug(TAG, "Value is different, will set the parameter");
            }
        } catch (ReaderException e) {
            LoggerUtil.error(TAG, "Get parameter exception: " + e);
            resultBundle = setBundleResult(RESULT_CODE_ERROR, e.getMessage());
            return resultBundle;
        }
        //endregion

        //region Set the parameter
        try {
            _reader.paramSet(key, value);
        } catch (ReaderException e) {
            LoggerUtil.error(TAG, "Set parameter exception: " + e);
            resultBundle = setBundleResult(RESULT_CODE_ERROR, e.getMessage());
            return resultBundle;
        }
        //endregion

        //region Verify the value is set
        try {
            Object currentValue = _reader.paramGet(key);

            if (!currentValue.toString().equals(value.toString())) {
                LoggerUtil.debug(TAG, String.format("Parameter %s is not set", key));
                setBundleResult(RESULT_CODE_ERROR, String.format("Parameter is not set", key));
                return resultBundle;
            }
        } catch (ReaderException e) {
            LoggerUtil.error(TAG, "Get parameter for verify exception: " + e);
            resultBundle = setBundleResult(RESULT_CODE_ERROR, e.getMessage());
            return resultBundle;
        }
        //endregion

        return resultBundle;
    }
    //endregion

    private void assertReader() throws Exception {
        if (_reader == null) {
            throw new Exception("Reader is null");
        }

        if (getConnectState() != ConnectState.Connected) {
            throw new Exception("Reader is not connected");
        }
    }

    public void firmwareLoad(InputStream inputStream) throws IOException, ReaderException {
        _reader.firmwareLoad(inputStream);
    }

    public Object executeTagOp(TagOp tagOp, TagFilter tagFilter) throws ReaderException {
        return _reader.executeTagOp(tagOp, tagFilter);
    }

    public boolean isConnected() {
        try {
            assertReader();
        } catch (Exception e) {
            LoggerUtil.debug(TAG, e.toString());
            return false;
        }

        return true;
    }

    public void setGunState(GunState gunState) {
        LoggerUtil.error(TAG, "Set gun state: " + gunState);
        this._gunState = gunState;
    }

    private GunState getGunState() {
        return this._gunState;
    }

    //region Cancel Functions

    /**
     * Cancel the connect/disconnect thread
     */
    public void cancel() {
        _cancel = true;
        if (getConnectState() == ConnectState.Connecting) {
            if (_reader != null) {
                _reader.destroy();
            }
        }
    }

    private boolean isCancel() {
        return _cancel;
    }

    private void resetCancel() {
        _cancel = false;
    }

    /**
     * To disconnect and power off reader
     */
    private void canceled() {
        LoggerUtil.debug(TAG, "Canceled()");

        //region Turn off RFID module's power
        try {
            PogoCtrl pogoCtrl = PogoCtrl.getInstance(_context);

            pogoCtrl.powerOff();
        } catch (final Exception ex) {
            LoggerUtil.error(TAG, "RFID Power Control get exception : " + ex.getMessage());
            onReaderErrorEvent(CommandType.PowerOff, ex.toString());
        }
        //endregion

        if (_reader != null) {
            _reader.destroy();
        }

        onReaderConnectStateChanged(ConnectState.Disconnected, null);
    }
    //endregion

    //region Get UART Path

    /**
     * Set the status about get UART path thread is finished or not
     *
     * @param finished True for finished, false for not
     */
    private void setGetUARTFinished(boolean finished) {
        _getUARTFinished = finished;
    }

    /**
     * Get the status of the get UART path thread is finished or not
     *
     * @return True for finished, false for not
     */
    private boolean getGetUARTFinished() {
        return _getUARTFinished;
    }

    /**
     * Create a thread to get UART path and save to _uartPath;
     */
    void getUARTPath() {
        setGetUARTFinished(false);
        if (executorGetUART == null || executorGetUART.isShutdown()) {
            executorGetUART = Executors.newSingleThreadExecutor();
        }
        executorGetUART.execute(new GetUARTThread());
    }

    public class GetUARTThread implements Runnable {
        @Override
        public void run() {
            LoggerUtil.info(TAG, "+++ GetUARTThread()");
            try {
                PogoCtrl pogoCtrl = PogoCtrl.getInstance(_context);

                Bundle b = pogoCtrl.getUartPath();
                if (b.getInt(BUNDLE_ERROR_CODE) == RESULT_CODE_SUCCESS) {
                    _uartPath = b.getString("UART");
                    LoggerUtil.debug(TAG, "UART: " + _uartPath);
                    _getUARTResult = true;
                } else {
                    LoggerUtil.error(TAG, "Get UART fail: " + b.getString(BUNDLE_ERROR_MSG));
                    onReaderErrorEvent(CommandType.UART, "Get UART fail: " + b.getString(BUNDLE_ERROR_MSG));
                }
            } catch (Exception e) {
                LoggerUtil.error(TAG, "Get UART exception: " + e);
                onReaderErrorEvent(CommandType.UART, "Get UART exception: " + e);
            }
            LoggerUtil.info(TAG, "--- GetUARTThread()");
            setGetUARTFinished(true);
        }
    }
    //endregion
}
