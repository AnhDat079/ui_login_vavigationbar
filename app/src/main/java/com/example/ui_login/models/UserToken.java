package com.example.ui_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserToken {

        @SerializedName("msg")
        @Expose
        public String msg;

        @SerializedName("token")
        @Expose
        public String token;
}
