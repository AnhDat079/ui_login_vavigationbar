package com.example.ui_login.models;

import java.util.List;

public class CategoryResponse {
    private boolean error;
    public List<Category> categories;

    public CategoryResponse(boolean error, List<Category> categories) {
        this.error = error;
        this.categories = categories;
    }

    public boolean isError() {
        return error;
    }

    public List<Category> getCategories() {
        return categories;
    }
}
