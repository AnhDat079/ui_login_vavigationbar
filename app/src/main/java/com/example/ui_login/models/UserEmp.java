package com.example.ui_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserEmp {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("shift")
    public Shift shift;

    public class Shift {
        @SerializedName("timeCheckIn")
        @Expose
        public String timeCheckIn;

        @SerializedName("timeCheckOut")
        @Expose
        public String timeCheckOut;
    }

}
