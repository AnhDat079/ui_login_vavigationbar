package com.example.ui_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("internalId")
    @Expose
    public String internalId;

//    @SerializedName("images")
//    @Expose
//    public List<Images> images;

//    public Category(List<Images> images) {
//        this.images = images;
//    }

    public Category(String name, String internalId) {
        this.name = name;
        this.internalId = internalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * String chuoiBanDau = "18-QTD-CAT-AAAD";
     * int viTriDauPhayThuHai = chuoiBanDau.indexOf('-', chuoiBanDau.indexOf('-') + 1);
     * String chuoiCanLay = chuoiBanDau.substring(0, viTriDauPhayThuHai);
     * System.out.print(chuoiCanLay);
     * https://itphutran.com/substring-trong-java-cat-chuoi-vi-du/
     */

    public String getInternalId() {
        int viTriDauPhayThuHai = internalId.indexOf('-', internalId.indexOf('-') + 1);
        String getInternalId = internalId.substring(0, viTriDauPhayThuHai);
        return getInternalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    /**
     * String base64String = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAA...";
     * String base64Image = base64String.split(",")[1];
     * byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
     * Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
     * imageView.setImageBitmap(decodedByte);
     * https://stackoverflow.com/questions/4837110/how-to-convert-a-base64-string-into-a-bitmap-image-to-show-it-in-a-imageview
     */
//    public Images getImages() {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            byte[] imageBytes = Base64.getDecoder().decode(images.getBuffer());
//            Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
//        }
//        return (Images) images;
//    }
//
//    public void setImages(Images images) {
//        this.images = (List<Images>) images;
//    }

//    public List<Images> getImages() {
//        List<Images>
//        return images;
//    }
//
//    public void setImages(List<Images> images) {
//        this.images = images;
//    }
}
