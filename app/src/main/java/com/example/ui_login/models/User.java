package com.example.ui_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("internalId")
    @Expose
    public String internalId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("phone")
    @Expose
    public String phone;
}
