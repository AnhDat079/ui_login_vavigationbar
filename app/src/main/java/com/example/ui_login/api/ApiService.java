package com.example.ui_login.api;

import com.example.ui_login.models.Category;
import com.example.ui_login.models.LoginActivity;
import com.example.ui_login.models.Stores;
import com.example.ui_login.models.User;
import com.example.ui_login.models.UserEmp;
import com.example.ui_login.models.UserToken;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiService {

    @POST("auth/login/")
    Call<UserToken> getUser(@Body LoginActivity user);

    //http://125.212.249.230:3001/api/v1/auth/profile
    @GET("auth/profile/")
    Call<User> UserProfile (@Header("Authorization") String token);

    @GET("auth/profile/")
    Call<UserEmp> UserEmpProfile (@Header("Authorization") String token);

    @POST("auth/signout/")
    Call<UserToken> SigOut (@Header("Authorization") String token);

    @GET("categories/")
    Call<List<Category>> getCategories(@Header("Authorization") String token);

    @GET("stores/")
    Call<List<Stores>> getListStore(@Header("Authorization") String token);

}
