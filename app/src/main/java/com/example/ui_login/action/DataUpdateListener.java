package com.example.ui_login.action;

public interface DataUpdateListener {
    void onDataUpdate(String mEPC);
}
