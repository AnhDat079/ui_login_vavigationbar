package com.example.ui_login.action;

public class HistogramData {

    int size;
    int value;
    String name;

    public HistogramData(int size) {
        this.size = size;
    }

    public void setData(int value, String name) {
        this.value = value;
        this.name = name;
    }

}
