package com.example.ui_login.action;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.ui_login.until.LoggerUtil;

import java.util.ArrayList;

public class SectionsStateAdapter extends FragmentStateAdapter {
    //==============================================================================================
    private static final String TAG = "SectionsAdapter";
    private ArrayList<PlaceholderFragment> arrayList = new ArrayList<>();

    //==============================================================================================
    public SectionsStateAdapter(FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    public void SetEPCString(String mEPCString) {
       LoggerUtil.debug(TAG, "SetEPCString");
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                LoggerUtil.debug(TAG, "arrayList " + i + " = " + arrayList.get(i));
                arrayList.get(i).onDataUpdate(mEPCString);
            }
        }
//        this.notifyDataSetChanged();
    }

    public String getEPCString(String titleName) {
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getTitle().equalsIgnoreCase(titleName)) {
                    return arrayList.get(i).getTargetEPC();
                }
            }
        }

        return "";
    }

    public void addFragment(PlaceholderFragment fragment) {
        arrayList.add(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        LoggerUtil.debug(TAG, "createFragment = " + position);
        return arrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

}

