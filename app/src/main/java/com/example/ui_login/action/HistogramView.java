package com.example.ui_login.action;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class HistogramView extends View {
    private static final String TAG = HistogramView.class.getSimpleName();
    public int w;
    public int h;
    private Paint paint = new Paint();

    private HistogramData data = null;

    public HistogramView(Context context) {
        super(context);

    }

    public HistogramView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HistogramView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    public HistogramView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    public void setData(HistogramData data) {
        this.data = data;
        if (null == data) {
            return;
        }
        invalidate();   //刷新UI
    }

    private static void setTextSizeForWidth(Paint paint, float desiredWidth,
                                            String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredWidth / bounds.width();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int lineLength = 5;
        //畫方格
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(lineLength);
        canvas.drawLine((float) (w * 0.4), h, (float) (w * 0.6), h, paint);      //画坐标系X轴
        canvas.drawLine((float) (w * 0.4), h, (float) (w * 0.4), (float) (h * 0.1), paint);    //画坐标系Y轴
        canvas.drawLine((float) (w * 0.6), h, (float) (w * 0.6), (float) (h * 0.1), paint);      //画坐标系X轴
        canvas.drawLine((float) (w * 0.4), (float) (h * 0.1), (float) (w * 0.6), (float) (h * 0.1), paint);    //画坐标系Y轴
        //確認資料的有無
        if (data == null) {
            return;
        }
        int size = data.size;
        int value = data.value;
        String count = data.name;
        if (value < 1) {
            return;
        }
        //畫格子
        float x = (float) (w * 0.5);
        float perValueHeight = (float) (h * 0.9) / size;
        float perValueWidth = (float) (w * 0.2) - lineLength;
        paint.setStrokeWidth(perValueWidth);
        paint.setStyle(Paint.Style.STROKE);
        float y0, y1 = 0;
        for (int i = 0; i < value; i++) {
            paint.setColor(Color.BLUE);
            y0 = h - ((float) lineLength / 2) - (perValueHeight * i);
            y1 = y0 - perValueHeight + lineLength;
            canvas.drawLine(x, y0, x, y1, paint);
        }
        //畫字
        setTextSizeForWidth(paint, perValueWidth , "-100dbm");
//        setTextSizeForWidth(paint, perValueWidth * 3 / 5, count);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText(count, (float) (w * 0.4), y1 - lineLength, paint);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.w = w - 10;
        this.h = h - 10;
    }


    public void initial() {
        this.data = null;
        invalidate();
    }
}

