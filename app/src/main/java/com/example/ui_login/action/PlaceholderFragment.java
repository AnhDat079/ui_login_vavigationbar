package com.example.ui_login.action;

import static com.example.ui_login.until.ActionName.ACTION_INITIAL;
import static com.example.ui_login.until.ActionName.ACTION_LOCATE_DATA;
import static com.example.ui_login.until.ActionName.ACTION_LOCATE_READING;
import static com.example.ui_login.until.ExtraName.LOCATE_RSSI;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.ui_login.R;
import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.until.ExtraName;
import com.thingmagic.ReaderException;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment implements DataUpdateListener {

    private static final String TAG = "SectionsFragment";
    private Context mContext;
    private final String _tabTitle;
    private ReaderActivity mReaderActivity;
    private Boolean startState = false;
    private HistogramData data = null;
    private int histogramSize = 20;
    private int histogramMax = -40;
    private int histogramMin = -80;
    private int histogramRange = histogramMax - histogramMin;


    public PlaceholderFragment(ReaderActivity readerActivity, String title) {
        _tabTitle = title.toUpperCase();
        mReaderActivity = readerActivity;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(mReaderActivity).unregisterReceiver(mBroadcastReceiver);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private LinearLayout layout_epc;
    private LinearLayout layout_bank;
    private LinearLayout layout_password;
    private LinearLayout layout_address;
    private LinearLayout layout_length;
    private LinearLayout layout_data;
    private LinearLayout layout_privilege;
    private LinearLayout layout_histogram;
    private EditText edt_epc;
    private EditText edt_password;
    private EditText edt_address;
    private EditText edt_length;
    private EditText edt_data;
    private TextView txt_bank;
    private Spinner spinner_bank;
    private Spinner spinner_privilege;
    private Spinner spinner_DataType;
    private Button btn_execute;
    private Button btn_start;
    private HistogramView histogram;



    private long NowTime() {
        return System.currentTimeMillis();
    }

    private int targetRSSI = -90;
    private long preBeepTime = 0;

    private void UpdateHistogram() {
        int size = (targetRSSI - histogramMax) / (histogramRange / histogramSize) + histogramSize;
        size = Math.min(size, histogramSize);
        long nowTime = NowTime();
        if (size > 0) {
            if (preBeepTime != 0) {
                if (targetRSSI > -40) {
                    preBeepTime = nowTime;
                    mReaderActivity._beeper.beepLocate(size, histogramSize);
                } else if (targetRSSI > -50) {
                    if (nowTime - preBeepTime > 100) {
                        preBeepTime = nowTime;
                        mReaderActivity._beeper.beepLocate(size, histogramSize);
                    }
                } else if (targetRSSI > -60) {
                    if (nowTime - preBeepTime > 300) {
                        preBeepTime = nowTime;
                        mReaderActivity._beeper.beepLocate(size, histogramSize);
                    }
                } else if (targetRSSI > -70) {
                    if (nowTime - preBeepTime > 500) {
                        preBeepTime = nowTime;
                        mReaderActivity._beeper.beepLocate(size, histogramSize);
                    }
                } else {
                    if (nowTime - preBeepTime > 800) {
                        preBeepTime = nowTime;
                        mReaderActivity._beeper.beepLocate(size, histogramSize);
                    }
                }
            } else {
                preBeepTime = nowTime;
                mReaderActivity._beeper.beepLocate(size, histogramSize);
            }

        }
        data.setData(size, targetRSSI + "dbm");

        histogram.setData(data);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_INITIAL:
                    initialView();
                    break;

                case ACTION_LOCATE_DATA:
//                    if (_tabTitle.equals("LOCATE") && startState) {
                    if (startState) {
//                        String epc = intent.getStringExtra(LOCATE_EPC);
                        int rssi = intent.getIntExtra(LOCATE_RSSI, histogramMin);
                        targetRSSI = rssi;
                        UpdateHistogram();
                    }
                    break;

                case ACTION_LOCATE_READING:
//                    if (_tabTitle.equals("LOCATE")) {
                        boolean startStatus = intent.getBooleanExtra(ExtraName.Start, false);
                        int startEnable = intent.getIntExtra(ExtraName.Enable, 1);
//                        if (!startStatus || startEnable != 0) {
                            setStartState(startStatus, startEnable);
//                        }
//                    }
                    break;
            }
        }
    };

    private int getBank() {
        int temp;
        switch (spinner_bank.getSelectedItem().toString()) {
            case "RESERVED":
                temp = 0;
                break;
            case "EPC":
                temp = 1;
                break;
            case "TID":
                temp = 2;
                break;
            case "USER":
                temp = 3;
                break;
            default:
                temp = -1;
                break;
        }
        return temp;
    }

    private String toASCII(String input) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            str.append(Integer.toHexString(c));
        }
        return str.toString();
    }

    private short[] toShorts(String finalString) {
        StringBuilder ChangeStr = new StringBuilder();
        for (int i = 0; i < finalString.length(); i++) {
            ChangeStr.append(finalString.charAt(i));
            if (i % 4 == 3) {
                ChangeStr.append(" ");
            }
        }
        String[] tabOfShortString = ChangeStr.toString().split(" ");
        int length = tabOfShortString.length;
        short[] shortsArray = new short[length];
        for (int i = 0; i < length; i++) {
            short res = (short) Integer.parseInt(tabOfShortString[i], 16);
            shortsArray[i] = res;
        }

        return shortsArray;
    }

    private void initialView() {
        if (edt_data != null) {
            edt_data.setText("");
        }
        setStartState(false, 1);
    }

    private void setStartState(boolean state, int enable) {
        if (btn_start == null) {
            return;
        }
        startState = state;
        if (state) {
            btn_start.setBackground(mReaderActivity.getDrawable(R.drawable.stop));
        } else {
            btn_start.setBackground(mReaderActivity.getDrawable(R.drawable.start));
        }
        switch (enable) {
            case 0:
                btn_start.setEnabled(false);
                btn_start.setClickable(false);
                break;
            case 1:
                btn_start.setEnabled(true);
                btn_start.setClickable(true);
                break;
        }

        if (data == null) {
            return;
        }
        data.setData(0, "0");
        histogram.setData(data);
        if (mReaderActivity.getReadBtnText().equals(getString(R.string.start_reading)) &&
                mReaderActivity.m6eReader != null &&
                mReaderActivity.m6eReader.isConnected() &&
                mReaderActivity.getConnectBtnText().equals(getString(R.string.Disconnect)) &&
                mReaderActivity.serviceListener != null &&
                startState) {
            //Harvey should setup the Inventory parameter for locate
            try {
                mReaderActivity.setTargetTag(edt_epc.getText().toString());
            } catch (ReaderException e) {
                e.printStackTrace();
            }

            mReaderActivity.serviceListener.startReading();
        }
        if (mReaderActivity.getReadBtnText().equals(getString(R.string.stop_reading)) &&
                mReaderActivity.m6eReader != null &&
                mReaderActivity.m6eReader.isConnected() &&
                mReaderActivity.getConnectBtnText().equals(getString(R.string.Disconnect)) &&
                mReaderActivity.serviceListener != null &&
                !startState) {
            mReaderActivity.serviceListener.stopReading();
        }

    }

    @Override
    public void onDataUpdate(String epc) {
        if (edt_epc != null) {
            edt_epc.setText(epc);
        }

        initialView();
    }

    public String getTargetEPC() {
        if (edt_epc != null) {
            return edt_epc.getText().toString();
        }
        return "";
    }

    public String getTitle() {
        return _tabTitle;
    }

}