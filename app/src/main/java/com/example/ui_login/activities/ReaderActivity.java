package com.example.ui_login.activities;

import static com.example.ui_login.until.ActionName.ACTION_LOCATE_DATA;
import static com.example.ui_login.until.ActionName.Action_SelectEPC;
import static com.example.ui_login.until.ActionName.BARCODE_DATA_ACTION;
import static com.example.ui_login.until.ActionName.BARCODE_DATA_LENGTH_ACTION;
import static com.example.ui_login.until.ExtraName.LOCATE_EPC;
import static com.example.ui_login.until.ExtraName.LOCATE_RSSI;
import static com.example.ui_login.until.Utilities.BUNDLE_ERROR_CODE;
import static com.example.ui_login.until.Utilities.BUNDLE_ERROR_MSG;
import static com.example.ui_login.until.Utilities.DEFAULT_TIMEOUT;
import static com.example.ui_login.until.Utilities.RESULT_CODE_SUCCESS;
import static com.example.ui_login.until.Utilities.sleep;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.ui_login.R;
import com.example.ui_login.action.PlaceholderFragment;
import com.example.ui_login.action.SectionsStateAdapter;
import com.example.ui_login.customViews.ExpandableListAdapter;
import com.example.ui_login.enums.ActionState;
import com.example.ui_login.enums.CommandType;
import com.example.ui_login.enums.ConnectState;
import com.example.ui_login.enums.GunState;
import com.example.ui_login.listener.ConnectionListener;
import com.example.ui_login.listener.FirmwareListener;
import com.example.ui_login.listener.SaveTagListener;
import com.example.ui_login.listener.ServiceListener;
import com.example.ui_login.reader.BaseReader;
import com.example.ui_login.reader.IReaderEventListener;
import com.example.ui_login.reader.M6eReader;
import com.example.ui_login.service.TagListAdapter;
import com.example.ui_login.until.ActionName;
import com.example.ui_login.until.Beeper;
import com.example.ui_login.until.ExtraName;
import com.example.ui_login.until.LoggerUtil;
import com.example.ui_login.until.RFIDConfig;
import com.example.ui_login.until.TimeoutCtrl;
import com.example.ui_login.until.Utilities;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.thingmagic.Gen2;
import com.thingmagic.ReaderException;
import com.thingmagic.SimpleReadPlan;
import com.thingmagic.TMConstants;
import com.thingmagic.TagData;
import com.thingmagic.TagFilter;
import com.thingmagic.TagProtocol;
import com.thingmagic.TagReadData;
import com.unitech.api.Key.KeySetting;
import com.unitech.util.Machine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ReaderActivity extends AppCompatActivity implements IReaderEventListener {
    private final String TAG = ReaderActivity.class.getSimpleName();

    private int KEYCODE_SCAN_EXTENDED = 290;

    //region Spinner Index
    private final int defaultMode = 2;
    private final int Custom_Mode = 3;

    private final int BLF_250K = 0;
    private final int BLF_640K = 1;

    private final int TARI_25 = 0;
    private final int TARI_12_5 = 1;
    private final int TARI_6_25 = 2;

    private final int Session_S0 = 0;
    private final int Session_S1 = 1;
    private final int Session_S2 = 2;
    private final int Session_S3 = 3;

    private final int Encoding_FM0 = 0;
    private final int Encoding_M2 = 1;
    private final int Encoding_M4 = 2;
    private final int Encoding_M8 = 3;

    private final int Target_AB = 0;
    private final int Target_BA = 1;
    private final int Target_A = 2;
    private final int Target_B = 3;

    private final int AntennaOnTimeMax = 1000;
    private final int AntennaOffTimeMin = 100;

    //region Button
    private Button connectButton = null;
    private Button scanButton = null;
    private Button clearButton = null;
    private Button tagOperationButton = null;
    private Button saveTagButton = null;

    //endregion

    private DisplayMetrics displaymetrics = new DisplayMetrics();

    //region TextView
    private TextView tableNumberView = null;
    private TextView tableEPCView = null;
    private TextView tableRSSIView = null;
    private TextView tableCountView = null;

    private TextView uniqueTagView = null;
    private TextView temperatureView = null;
    private TextView avgReadView = null;
    private TextView totalTagCountView = null;
    private TextView readingTimeView = null;
    private TextView errorMsgView = null;
    private TextView maxReadRateView = null;

    private TextView appVersionView = null;
    //endregion

    //region Spinner
    private Spinner regionList = null;
    //endregion

    //region Reader Option
    private Spinner operationModeList = null;
    private Spinner powerList = null;

    private EditText readTimeoutView = null;
    private EditText antennaOnTimeView = null;
    private EditText antennaOffTimeView = null;

    private Spinner blfList = null;
    private Spinner tariList = null;
    private Spinner sessionList = null;
    private Spinner encodingList = null;
    private Spinner targetList = null;
    private Spinner qList = null;
    private Switch switchFastSearch = null;
    //endregion

    //region Firmware
    private TextView softwareVersionView = null;
    private TextView hardwareVersionView = null;
    private TextView modelVersionView = null;
    private TextView productGroupVersionView = null;
    private TextView serialNumberView = null;
    private LinearLayout firmware_hint = null;
    private LinearLayout layout_firmware = null;
    //endregion

    //    private TableLayout tagTable = null;
    private RecyclerView tagListView = null;
    public TagListAdapter tagListAdapter = null;

    private int windowWidth;
    public int tableNumberWidth = 0;
    public int tableEPCWidth = 0;
    public int tableRSSIWidth = 0;
    public int tableCountWidth = 0;

    public View readOptions;
    public View firmware;
    public View about;
    public View mActionView;

    private LinearLayout read_result_layout = null;

    public com.example.ui_login.customViews.ExpandableListAdapter expandableListAdapter;

    private CheckBox checkBeep = null;
    public CheckBox enableBarcode = null;
    public EditText barcodeText = null;

    /**
     * The flag indicating whether content is loaded (text is shown) or not (setting view is
     * shown).
     */

    private View mSettingView;
    private View mDisplayView;

    public ServiceListener serviceListener;
    private FirmwareListener firmwareListener;

    public Beeper _beeper;

    private boolean onPauseTrigger = false;

    private boolean _targetGunForRFID = true;

    private int skipCount = 0;

    private RFIDConfig properties;

    private boolean firstExecute = true;
    private boolean defaultKeyRemapEnable = true;

    private boolean isOnPauseTrigger = false;

    private int getSkipCount = 0;


    //region Tag Operation
    public com.example.ui_login.action.SectionsStateAdapter sectionsStateAdapter = null;
    public TabLayout tabLayout = null;
    public ViewPager2 viewPager = null;
    public int ActionTabPosition = 0;
    private boolean initTagOperationFlag = false;

    public String targetEPC = "";

    private ProgressDialog keyRemapDialog;
    private ProgressDialog pDialog;
    SetGunKeyViaResumePauseThread remapThread = null;
    //endregion

    private String FILE_FOLDER = "";
    private String FILE_PATH_KEYS_CONFIG = "";
    private KeySetting keySetting;

    public static String PACKAGE_NAME;

    //    private ReentrantLock lockTags = new ReentrantLock();
    public M6eReader m6eReader;
    private boolean checkPermissionFinished = false;
    //


    Toolbar toolbar;

//==============================================================================================

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_start);

            toolbar = findViewById(R.id.toolbarStar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Start");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //Check permission
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ReaderActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else {
                checkPermissionFinished = true;
            }
            //--------------------------------------------------------------------------------------
            LoggerUtil.initialize(this);
            String path = String.format("%s/%s", this.getExternalFilesDir(null).getAbsolutePath(), LoggerUtil.DIR_LOG);
            File file = new File(path);
            if (!file.exists()) {
                if (file.mkdir()) {
                    LoggerUtil.debug(TAG, "Build RFIDReader Log File.");
                }
            }
            //--------------------------------------------------------------------------------------
            keyRemapDialog = new ProgressDialog(this);
            keyRemapDialog.setCancelable(false);
            keyRemapDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            keyRemapDialog.setMessage("Switching gun key");
            remapThread = new SetGunKeyViaResumePauseThread(this);

            FILE_FOLDER = String.format("%s/", this.getApplicationContext().getExternalFilesDir(null).getAbsolutePath());
            FILE_PATH_KEYS_CONFIG = String.format("%skeys_config.txt", FILE_FOLDER);
            ReaderActivity activity = this;
            try {
                Utilities.loadLog4j(this);
            }catch (Exception ex) {
                LoggerUtil.error(TAG, "Error Loading log4j",ex);
            }


            properties = new RFIDConfig(this.getApplicationContext());


            tagOperationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getConnectBtnText().equals(getString(R.string.Disconnect))) {
                        sectionsStateAdapter.SetEPCString("");
//                        sectionsStateAdapter.notifyDataSetChanged();
                        showSettingsOrTagsView(2);
                    }
                }
            });

            connectButton.setOnClickListener(new ConnectionListener(this));
            clearButton.setOnClickListener(clearListener);
            saveTagButton.setOnClickListener(new SaveTagListener(this));

            serviceListener = new ServiceListener(this);
            scanButton.setOnClickListener(serviceListener);

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> regionadapter = ArrayAdapter.createFromResource(this, R.array.regions_array, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            regionadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            regionList.setAdapter(regionadapter);
            regionList.setSelection(15);

            tagListAdapter = new TagListAdapter(getApplicationContext(), this);

            tagListView.setLayoutManager(new LinearLayoutManager(this));
            tagListView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            tagListView.setAdapter(tagListAdapter);

            firmwareListener = new FirmwareListener(this);

            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            loadReadOptionFromProperties();

            setPortraitTableLayout();
            System.gc();

            initReader();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        createBeeper();

    }

    @Override
    protected void onResume() {
        LoggerUtil.error(TAG, "+++ onResume()");
        if (!checkPermissionFinished) {
            super.onResume();
            return;
        }
        scanServiceReceiverCtrl(true);
        localReceiverCtrl(true);
        systemReceiverCtrl(true);

//        PackageManager localPackageManager = getPackageManager();
//        Intent intent = new Intent("android.intent.action.MAIN");
//        intent.addCategory("android.intent.category.HOME");
//        String str = localPackageManager.resolveActivity(intent,
//                PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
//        Log.e(TAG, "Current launcher Package Name:" + str);
//        String temp = getTopActivityName();
//        Log.e(TAG, "Top activity name:" + temp);

        if (Machine.EA630Plus) {
            //Change the key to internal trigger key to control barcode/RFID
            setTargetKey(!isEnableBarcode());
            setGunForRFID();

            initReader();

//            RFIDModulePowerCtrl(true);
        } else {

            if (remapThread.getStatus() == AsyncTask.Status.RUNNING) {
                LoggerUtil.debug(TAG, "remapThread is running");

                if (onPauseTrigger) {
                    onPauseTrigger = false;
                    setTargetKey(!isEnableBarcode());
                    LoggerUtil.debug(TAG, "Change target gun to " + (isTargetGunForRFID() ? "RFID" : "Scanner"));
                }
            } else {
                LoggerUtil.debug(TAG, "remapThread is not running");
                onPauseTrigger = false;

                if (skipCount > 0) {
                    LoggerUtil.debug(TAG, "skipCount is not 0");
                    skipCount--;
                } else {
                    //Change the key to internal trigger key to control barcode/RFID
                    setTargetKey(!isEnableBarcode());
                    setGunForRFID();
//                    final Bundle result = RFIDModulePowerCtrl(true);
//                    if (result.getInt(BUNDLE_ERROR_CODE) == RESULT_CODE_ERROR){
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                showErrorMsg(true);
//                                setErrorMsgTextColor(0xffff0000);
//                                setErrorMsg(result.getString(BUNDLE_ERROR_MSG));
//                            }
//                        });
//                    }
                }
            }
        }
        super.onResume();

        LoggerUtil.error(TAG, "--- onResume()");
    }

    @Override
    protected void onPause() {
        LoggerUtil.error(TAG, "+++ onPause()");
        if (!checkPermissionFinished) {
            super.onPause();
            return;
        }
        if (firmwareListener != null && firmwareListener.isFirmwareUpgrading()) {
            super.onPause();
            return;
        }

        scanServiceReceiverCtrl(false);
        localReceiverCtrl(false);
        systemReceiverCtrl(false);

        String strLauncherName = getLauncherName();
        String temp = getTopActivityName();

        if (temp.equalsIgnoreCase(strLauncherName) || Machine.EA630Plus) {
            if (m6eReader != null) {
                if (serviceListener != null && m6eReader.getActionState() == ActionState.Inventory) {
                    serviceListener.stopReading();
                    serviceListener.clearTagRecords();
                }
            }
            onPauseTrigger = true;
            setTargetKey(false);
            setGunForRFID();

            if (m6eReader != null) {
                m6eReader.disconnect();

                //Destroy will remove listener
                //So make it to null then re-initial
                m6eReader.destroy();
                m6eReader = null;
            }

            onReaderConnectStateChanged(null, ConnectState.Disconnected, null);
        } else if (temp.equalsIgnoreCase(PACKAGE_NAME)) {
            if (remapThread.getStatus() == AsyncTask.Status.RUNNING) {
            } else {
                if (skipCount == 0) {
                    if (m6eReader != null) {
                        if (serviceListener != null && m6eReader.getActionState() == ActionState.Inventory) {
                            serviceListener.stopReading();
                            serviceListener.clearTagRecords();
                        }
                    }
                    onPauseTrigger = true;
                    setTargetKey(false);
                    setGunForRFID();

                    //Destroy will remove listener
                    //So make it to null then re-initial
                    if (m6eReader != null) {
                        m6eReader.disconnect();

                        m6eReader.destroy();
                        m6eReader = null;
                    }

                    onReaderConnectStateChanged(null, ConnectState.Disconnected, null);
                }
            }
        }
        super.onPause();
        LoggerUtil.error(TAG, "--- onPause()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FirmwareListener.FILE_SELECT_CODE:

                if (resultCode == RESULT_OK) {
                    firmwareListener.onFileSelectorResult(data);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            checkPermissionFinished = true;
            return;
        }
    }

    //==============================================================================================
    @SuppressLint("InflateParams")
    private void loadSettingContentView() {

        LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert infalInflater != null;
        ArrayList<View> ChildViews = new ArrayList<>();
        ChildViews.add(readOptions);
        ChildViews.add(firmware);
        ChildViews.add(about);

        String[] groups = {"Read Options", "Firmware", "About"};
        expandableListAdapter = new  ExpandableListAdapter(this, groups, ChildViews);
    }

    public void updateSettings() {
        updateRFIDVersion();
        setupReader();
    }

    /**
     * Save Read Options from UI to properties
     */
    public void saveReadOptionToProperties() {
        LoggerUtil.debug(TAG, "saveReadOptionToProperties");

        properties.setOperationMode(operationModeList.getSelectedItemPosition());

        if (operationModeList.getSelectedItemPosition() != Custom_Mode) {
            return;
        }

        properties.setReaderTimeout(Integer.parseInt(readTimeoutView.getText().toString()));

        properties.setAntennaOnTime(Integer.parseInt(antennaOnTimeView.getText().toString()));
        properties.setAntennaOffTime(Integer.parseInt(antennaOffTimeView.getText().toString()));

        properties.setPower(powerList.getSelectedItemPosition());

        properties.setBLF(blfList.getSelectedItemPosition());

        properties.setTARI(tariList.getSelectedItemPosition());

        properties.setSession(sessionList.getSelectedItemPosition());

        properties.setEncoding(encodingList.getSelectedItemPosition());

        properties.setTarget(targetList.getSelectedItemPosition());

        properties.setQValue(qList.getSelectedItemPosition());

        properties.setFastSearch(switchFastSearch.isChecked());
    }

    /**
     * Change Read Option to current properties setting
     */
    private void loadReadOptionFromProperties() {
        LoggerUtil.debug(TAG, "loadReadOptionFromProperties");
        operationModeList.setSelection(properties.getOperationMode());

        loadReadOptionFromProperties(properties.getOperationMode());
    }

    /**
     * Change the Read Option with specific mode from properties
     *
     * @param mode The Operation Mode
     */
    private void loadReadOptionFromProperties(int mode) {
        LoggerUtil.debug(TAG, "loadReadOptionFromProperties with mode " + mode);
        readTimeoutView.setText(String.valueOf(properties.getReaderTimeout(mode)));

        antennaOnTimeView.setText(String.valueOf(properties.getAntennaOnTime(mode)));
        antennaOffTimeView.setText(String.valueOf(properties.getAntennaOffTime(mode)));

        powerList.setSelection(properties.getPower(mode));

        blfList.setSelection(properties.getBLF(mode));

        tariList.setSelection(properties.getTARI(mode));

        sessionList.setSelection(properties.getSession(mode));

        encodingList.setSelection(properties.getEncoding(mode));

        targetList.setSelection(properties.getTarget(mode));

        qList.setSelection(properties.getQValue(mode));

        switchFastSearch.setChecked(properties.getFastSearch(mode));

        lockReaderSetting(mode == Custom_Mode);
    }

    public void lockReaderSetting(boolean enable) {
        readTimeoutView.setEnabled(enable);
        antennaOnTimeView.setEnabled(enable);
        antennaOffTimeView.setEnabled(enable);
        powerList.setEnabled(enable);
        blfList.setEnabled(enable);

        if (operationModeList.getSelectedItemPosition() == Custom_Mode && blfList.getSelectedItemPosition() != BLF_640K)
            tariList.setEnabled(true);
        else
            tariList.setEnabled(false);

        sessionList.setEnabled(enable);
        encodingList.setEnabled(enable);
        targetList.setEnabled(enable);
        qList.setEnabled(enable);
        switchFastSearch.setEnabled(enable);

        if (enable) {
            if (Integer.parseInt(antennaOffTimeView.getText().toString()) < AntennaOffTimeMin) {
                LoggerUtil.debug(TAG, "Change OffTime to " + R.string.antennaOffTimeMin);
                antennaOffTimeView.setText(R.string.antennaOffTimeMin);
            }
        }
    }

    public void closeSettings() {
        showSettingsOrTagsView(0);
    }

    /**
     * Show/Hide the content
     *
     * @param contentLoaded 0 for Main, 1 for Setting, 2 for Action
     */
    public void showSettingsOrTagsView(int contentLoaded) {
        // Decide which view to hide and which to show.
        switch (contentLoaded) {
            case 0:
                mDisplayView.setVisibility(View.VISIBLE);
                mSettingView.setVisibility(View.GONE);
                mActionView.setVisibility(View.GONE);
                showSaveTagBtn(true);
                break;
            case 1:
                mDisplayView.setVisibility(View.GONE);
                mSettingView.setVisibility(View.VISIBLE);
                mActionView.setVisibility(View.GONE);
                showSaveTagBtn(false);
                break;
            case 2:
                mDisplayView.setVisibility(View.GONE);
                mSettingView.setVisibility(View.GONE);
                mActionView.setVisibility(View.VISIBLE);
                showSaveTagBtn(false);
                break;
        }
    }

    /**
     * This is called when the screen rotates.
     * (onCreate is no longer called when screen rotates due to manifest, see: android:configChanges)
     */
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orientation = newConfig.orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setPortraitTableLayout();
        } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setPortraitTableLayout();
        }
    }

    private void setPortraitTableLayout() {
        tableNumberWidth = (int) (windowWidth * (0.12));
        tableEPCWidth = (int) (windowWidth * (0.6));
        tableRSSIWidth = (int) (windowWidth * (0.12));
        tableCountWidth = (int) (windowWidth * (0.16));

        tableNumberView.setWidth(tableNumberWidth);
        tableEPCView.setWidth(tableEPCWidth);
        tableRSSIView.setWidth(tableRSSIWidth);
        tableCountView.setWidth(tableCountWidth);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isEnableBarcode()) {
            return super.onKeyDown(keyCode, event);
        }

        LoggerUtil.error(TAG, "Get Key Code: " + keyCode);

        if (keyCode == KEYCODE_SCAN_EXTENDED) {
            if (m6eReader != null && ReaderActivity.this.m6eReader.isConnected() && getConnectBtnText().equals(getString(R.string.Disconnect))
                    && serviceListener != null && m6eReader.getActionState() == ActionState.Stop) {
                //Harvey should setup the Inventory parameter for locate
                if (mActionView.getVisibility() == View.VISIBLE) {
                    if (ActionTabPosition != 4) {
                        return true;
                    }
                    String targetTag = sectionsStateAdapter.getEPCString("locate");

                    if (targetTag.trim().equalsIgnoreCase("")) {
                        Toast.makeText(this, "Please input target tag!", Toast.LENGTH_SHORT).show();
                        return true;

                    } else {
                        try {
                            setTargetTag(targetTag);
                        } catch (ReaderException e) {
                            e.printStackTrace();
                        }
                    }

                    Intent intent = new Intent(ActionName.ACTION_LOCATE_READING);
                    intent.putExtra(ExtraName.Start, true);
                    intent.putExtra(ExtraName.Enable, 1);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                } else if (mDisplayView.getVisibility() == View.VISIBLE) {
                    serviceListener.startReading();
                }
            }
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (isEnableBarcode()) {
            return super.onKeyLongPress(keyCode, event);
        }

        if (keyCode == KEYCODE_SCAN_EXTENDED) {
            if (ReaderActivity.this.m6eReader.isConnected() && getConnectBtnText().equals(getString(R.string.Disconnect))
                    && serviceListener != null && m6eReader.getActionState() == ActionState.Stop) {
                //Harvey should setup the Inventory parameter for locate
//                    if (mActionView.getVisibility() == View.VISIBLE) {
//                        setupReaderForLocate();
//                    }
//                    serviceListener.startReading();
            }
            return true;
        } else {
            return super.onKeyLongPress(keyCode, event);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (isEnableBarcode()) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == KEYCODE_SCAN_EXTENDED) {
            if (mActionView.getVisibility() == View.VISIBLE) {
                if (ActionTabPosition != 4) {
                    return true;
                }
                Intent intent = new Intent(ActionName.ACTION_LOCATE_READING);
                intent.putExtra(ExtraName.Start, false);
                intent.putExtra(ExtraName.Enable, 1);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            } else if (mDisplayView.getVisibility() == View.VISIBLE) {
                serviceListener.stopReading();
            }

            return true;
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    private void findAllViewsById() {
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        //region Button
        connectButton = findViewById(R.id.btn_connect);
        scanButton = findViewById(R.id.btn_scan);
        clearButton = findViewById(R.id.btn_Clear);
        saveTagButton = findViewById(R.id.btn_savetag);
        //endregion

        //region TextView
        uniqueTagView = findViewById(R.id.search_result_view);
        tableNumberView = findViewById(R.id.title_no);
        tableEPCView = findViewById(R.id.title_epc);
        tableRSSIView = findViewById(R.id.title_rssi);
        tableCountView = findViewById(R.id.title_count);

        temperatureView = findViewById(R.id.temperature);
        avgReadView = findViewById(R.id.avg_reads_view);
        totalTagCountView = findViewById(R.id.totalTagCount_view);
        readingTimeView = findViewById(R.id.reading_time);
        errorMsgView = findViewById(R.id.error_messages_view);
        maxReadRateView = findViewById(R.id.max_read_rate);
        //endregion

        read_result_layout = findViewById(R.id.read_result_layout);


        checkBeep = findViewById(R.id.BeepControl);
        checkBeep.setOnCheckedChangeListener(beeperCheckedListener);

        enableBarcode = findViewById(R.id.BarcodeControl);
        enableBarcode.setOnCheckedChangeListener(scanServiceCheckedListener);
        barcodeText = findViewById(R.id.barcodeText);
        barcodeText.setRawInputType(InputType.TYPE_NULL);

//        tableTags = findViewById(R.id.table_tags);
//        tagTable = findViewById(R.id.tablelayout);
        tagListView = findViewById(R.id.recycler_view);


        //-----------------------------------------------------------------------------------------
    }

    @Override
    public void onReaderActionChanged(BaseReader reader, ActionState actionState, Object params) {
        updateUI();
        switch (actionState) {
            case Stop:
                if (serviceListener != null) {
                    serviceListener.stopReading();
                }

                _beeper.stopAllBeep();
                beepCheckBoxEdit(true);

                tagOpBtnEdit(true);
                saveTagBtnEdit(true);

                break;
            case Inventory:
                tagOpBtnEdit(false);
                saveTagBtnEdit(false);
                beepCheckBoxEdit(false);

//                setUniqueTagTextColor(COLOR_TEXT);
//                setSearchResultCountText(mReaderActivity.getResources().getString(R.string.reading_tags));
                break;
            case Inventorying:
                serviceListener.clearTagRecords();
                break;
            case Stopping:
                break;
        }
    }

    public void initReader() {
        if (m6eReader == null) {
            m6eReader = new M6eReader(this);
            m6eReader.addListener(this);
        }
    }

    @Override
    public void onReaderConnectStateChanged(BaseReader reader, ConnectState connectState, Object params) {
        LoggerUtil.error(TAG, "Connect State Changed: " + connectState);
        updateUI();
        switch (connectState) {
            case Disconnected:
                if (pDialog != null) {
                    pDialog.dismiss();
                }

                if (keyRemapDialog != null) {
                    keyRemapDialog.dismiss();
                }

                clearInventoryResult();

                showLayoutFirmware(false);
                showFirmwareHint(true);
                saveTagBtnEdit(false);
                tagOpBtnEdit(false);
                regionListEdit(true);
                System.gc();

//                this.cancel(false);
                break;
            case Connecting:

                if (pDialog != null) {
                    pDialog.setMessage("Connecting. Please wait...");
                    pDialog.show();
                }

                if (keyRemapDialog != null) {
                    keyRemapDialog.dismiss();
                }

                saveTagBtnEdit(false);
                clearInventoryResult();
                setErrorMsg("");
                showErrorMsg(false);
                break;
            case Connected:
                if (pDialog != null) {
                    pDialog.dismiss();
                }

                clearInventoryResult();

                saveTagBtnEdit(true);
                tagOpBtnEdit(true);
                regionListEdit(false);

                updateSettings();
                break;
            case Disconnecting:
                if (pDialog != null) {
                    pDialog.setMessage("Disconnecting. Please wait...");
                    pDialog.show();
                }
                saveTagBtnEdit(false);
                tagOpBtnEdit(false);
                break;
        }
    }

    @Override
    public void onReaderReadTag(BaseReader reader, TagReadData tagReadData) {

        String epcString = tagReadData.getTag().epcString();
//            Log.d(TAG, String.format("parseTag: %d read count, epc='%s'", tr.getReadCount(), tr.getTag().epcString()));
        if (mActionView.getVisibility() != View.VISIBLE) {
            if (tagReadData.getReadCount() > 0) {
                _beeper.beep();
            }
            serviceListener.lockTags.lock();
            if (serviceListener.epcToReadDataMap.keySet().contains(epcString)) {
                TagRecord tempTR = serviceListener.epcToReadDataMap.get(epcString);
                tempTR.setRssi(tagReadData.getRssi());
                tempTR.readCount += tagReadData.getReadCount();
            } else {
                TagRecord tagRecord = new TagRecord();
                tagRecord.setEpcString(epcString);
                tagRecord.setReadCount(tagReadData.getReadCount());
                tagRecord.setRssi(tagReadData.getRssi());
                serviceListener.epcToReadDataMap.put(epcString, tagRecord);
            }
            serviceListener.lockTags.unlock();

        } else {

            if (ActionTabPosition == 4) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_LOCATE_DATA)
                        .putExtra(LOCATE_EPC, epcString)
                        .putExtra(LOCATE_RSSI, tagReadData.getRssi()));
            }
        }
    }

    @Override
    public void onReaderErrorEvent(BaseReader reader, CommandType commandType, String msg) {
        LoggerUtil.error(TAG, "[onReaderErrorEvent] type: " + commandType + ", msg: " + msg);

        showErrorMsg(true);
        setErrorMsgTextColor(0xffff0000);
        setErrorMsg(msg);
    }

    @Override
    public void onReaderTemperatureChanged(BaseReader reader, int temperature) {
        // Update temperature
        if (temperature > 70)
            setTemperatureText(Html.fromHtml("<b>Temperature:</b> " + "<font color=\"red\">" + temperature + "</font>"));
        else
            setTemperatureText(Html.fromHtml("<b>Temperature:</b> " + temperature));
    }

    private class PositionOnTabSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override
        public void onTabSelected(@NonNull TabLayout.Tab tab) {
            ActionTabPosition = tab.getPosition();
            LoggerUtil.debug(TAG, "ActionTabPosition = " + ActionTabPosition);
            setupReaderForLocate();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            // No-op
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            // No-op
        }
    }


    private void initTagOperation() {
        initTagOperationFlag = true;
        final String[] tab_text_array = getResources().getStringArray(R.array.tab_text_array);
        tabLayout.setTabTextColors(Color.WHITE, Color.WHITE);
        tabLayout.setSelectedTabIndicatorColor(Color.rgb(255, 128, 128));
        tabLayout.addOnTabSelectedListener(new PositionOnTabSelectedListener());
        TabLayoutMediator.TabConfigurationStrategy tabConfigurationStrategy = new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(tab_text_array[position]);
            }
        };
        sectionsStateAdapter = new  SectionsStateAdapter(getSupportFragmentManager(), getLifecycle());
        for (String s : tab_text_array) {
            sectionsStateAdapter.addFragment(new PlaceholderFragment(this, s));
        }
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(sectionsStateAdapter);
        new TabLayoutMediator(tabLayout, viewPager, tabConfigurationStrategy).attach();
    }

    private void initUIReadOption() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> poweradapter = ArrayAdapter.createFromResource(this, R.array.powerlevel_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        poweradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        powerList.setAdapter(poweradapter);
        powerList.setSelection(20);


        operationModeList.setOnItemSelectedListener(operationModeSelectedListener);
        operationModeList.setSelection(defaultMode);

        antennaOnTimeView.setOnFocusChangeListener(asyncOnTimeFocusChangeListener);
        antennaOffTimeView.setOnFocusChangeListener(asyncOffTimeFocusChangeListener);

    }

    private void updateUI() {
        updateBtnRead();
        updateBtnConnect();
        updateCheckBoxBarcode();
    }

    /**
     * Update the Read button's UI
     */
    private void updateBtnRead() {
        if (isEnableBarcode()) {
            setReadBtnText(getString(R.string.start_reading));
            readBtnEdit(false);
        } else {
            if (m6eReader == null) {
                setReadBtnText(getString(R.string.start_reading));
                readBtnEdit(false);
            } else {
                if (!m6eReader.isConnected()) {
                    setReadBtnText(getString(R.string.start_reading));
                    readBtnEdit(false);
                } else {
                    switch (m6eReader.getActionState()) {
                        case Stop:
                            readBtnEdit(true);
                            setReadBtnText(getString(R.string.start_reading));
                            break;
                        case Inventory:
                            readBtnEdit(true);
                            setReadBtnText(getString(R.string.stop_reading));
                            break;
                        case Inventorying:
                            readBtnEdit(false);
                            setReadBtnText(getString(R.string.reading_tags));
                            break;
                        case Stopping:
                            readBtnEdit(false);
                            setReadBtnText(getString(R.string.stopping));
                            break;
                    }
                }
            }
        }
    }

    /**
     * Update the Connect button's UI
     */
    private void updateBtnConnect() {

        if (isEnableBarcode()) {
            setConnectBtnText(getString(R.string.Connect));
            connectBtnEdit(false);
        } else {
            if (m6eReader == null) {
                setConnectBtnText(getString(R.string.Connect));
                connectBtnEdit(true);
            } else {
                switch (m6eReader.getConnectState()) {
                    case Disconnected:
                        setConnectBtnText(getString(R.string.Connect));
                        connectBtnEdit(true);
                        break;
                    case Connecting:
                    case Disconnecting:
                        connectBtnEdit(false);
                        break;
                    case Connected:
                        setConnectBtnText(getString(R.string.Disconnect));
                        connectBtnEdit(true);
                        break;
                }
            }
        }
    }

    /**
     * Update the Barcode check box's UI
     */
    private void updateCheckBoxBarcode() {
        if (m6eReader == null) {
            setEnableBarcode(true);
        } else {
            switch (m6eReader.getConnectState()) {
                case Disconnected:
                    setEnableBarcode(true);
                    break;
                case Connecting:
                case Disconnecting:
                    setEnableBarcode(false);
                    break;
                case Connected:
                    switch (m6eReader.getActionState()) {
                        case Stop:
                            setEnableBarcode(true);
                            break;
                        case Inventory:
                        case Inventorying:
                        case Stopping:
                            setEnableBarcode(false);
                            break;
                    }
                    break;
            }
        }
    }

    //region UI Listener
    private View.OnFocusChangeListener asyncOnTimeFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (Integer.parseInt(antennaOnTimeView.getText().toString()) > AntennaOnTimeMax) {
                    LoggerUtil.debug(TAG, "Change OnTime to " + R.string.antennaOnTimeMax);
                    antennaOnTimeView.setText(R.string.antennaOnTimeMax);
                }
            }
        }
    };

    private View.OnFocusChangeListener asyncOffTimeFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (Integer.parseInt(antennaOffTimeView.getText().toString()) < AntennaOffTimeMin) {
                    LoggerUtil.debug(TAG, "Change OffTime to " + R.string.antennaOffTimeMin);
                    antennaOffTimeView.setText(R.string.antennaOffTimeMin);
                }
            }
        }
    };

    private AdapterView.OnItemSelectedListener operationModeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            loadReadOptionFromProperties(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener blfSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case BLF_250K:
                    if (operationModeList.getSelectedItemPosition() == Custom_Mode) {
                        tariList.setEnabled(true);
                    }
                    break;
                case BLF_640K:
                    if (operationModeList.getSelectedItemPosition() == Custom_Mode) {
                        tariList.setSelection(TARI_6_25);
                        tariList.setEnabled(false);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private CheckBox.OnCheckedChangeListener beeperCheckedListener = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                _beeper.setCanBeep(true);
                _beeper.stopAllBeep();
                _beeper.tryBeep();
            } else {
                _beeper.stopAllBeep();
                _beeper.setCanBeep(false);
            }
        }
    };

    private CheckBox.OnCheckedChangeListener scanServiceCheckedListener = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            setTargetKey(!isChecked);
            setGunKeyViaBarcodeCheckBox(!isChecked);

            if (isChecked) {
                saveTagBtnEdit(false);

                barcodeText.setEnabled(true);
                barcodeText.setVisibility(View.VISIBLE);
                barcodeText.requestFocus();
                barcodeText.setShowSoftInputOnFocus(false);

                if (m6eReader != null) {
                    m6eReader.disconnect();
                }

                tagOperationButton.setEnabled(false);

                if (getReadBtnText().equals(getString(R.string.start_reading)) && scanButton.isEnabled()) {
                    scanButton.setEnabled(false);
                    scanButton.setTag(Boolean.TRUE);
                }

                tagListView.setVisibility(View.GONE);
//                tableTags.setVisibility(View.GONE);

                if (read_result_layout.getVisibility() == View.VISIBLE) {
                    read_result_layout.setVisibility(View.GONE);
                    read_result_layout.setTag(Boolean.TRUE);
                }

            } else {
                barcodeText.setEnabled(false);
                barcodeText.setVisibility(View.GONE);
                tagOperationButton.setEnabled(true);

                if (m6eReader != null) {
                    m6eReader.connect(getSelectedRegionList());
                }

                tagListView.setVisibility(View.VISIBLE);
//                tableTags.setVisibility(View.VISIBLE);

                if (read_result_layout.getTag() == Boolean.TRUE) {
                    read_result_layout.setTag(Boolean.FALSE);
                    read_result_layout.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    public View.OnClickListener clearListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isEnableBarcode()) {
                clearBarcodeText();
            } else {
                serviceListener.clearTagRecords();
            }
        }
    };

    //endregion

    public boolean createDirectory(String pathName) {
        File file = new File(pathName);

        if (file.exists())
            return true;
        else {
            if (file.mkdirs())
                return true;
            else {
                if (createDirectory(file.getParent()))
                    return file.mkdirs();
                else
                    return false;
            }
        }
    }

    //region UI Control Functions

    /**
     * Reset the UI to clear
     */
    public void clearInventoryResult() {
        setUniqueTagText("");
        setTemperatureText("");
        setAvgReadText("");
        setTotalTagCountText("");
        setReadingTimeText("");
        setMaxReadRate("");
//        setErrorMsg("");

        resetTagTable();
    }

    public void resetTagTable() {
//        tagTable.removeAllViews();
//        ArrayList<TagRecord> tr = new ArrayList<>();
//        TagListAdapter tags = new TagListAdapter(getApplicationContext(), this, tr);
//        tagListView.setAdapter(tags);
        tagListAdapter.clear();
    }

    public void UpdateTagList(ConcurrentHashMap<String, TagRecord> tagList) {
        //        tagListAdapter.UpdateData(data);
//        TagListAdapter tagListAdapter = new TagListAdapter(getApplicationContext(), this, data);
//        tagListView.setAdapter(tagListAdapter);
        tagListAdapter.add(tagList);
    }


    public void clearBarcodeText() {
        this.barcodeText.getText().clear();
    }

    //region Connect Button
    public void connectBtnEdit(boolean enable) {
        connectButton.setEnabled(enable);
        connectButton.setClickable(enable);
    }

    public void setConnectBtnText(String text) {
        connectButton.setText(text);
    }

    public String getConnectBtnText() {
        return connectButton.getText().toString();
    }
    //endregion

    //region Read Button
    public void readBtnEdit(boolean enable) {
        scanButton.setEnabled(enable);
        scanButton.setClickable(enable);
    }

    public void setReadBtnText(String text) {
        scanButton.setText(text);
    }

    public String getReadBtnText() {
        return scanButton.getText().toString();
    }
    //endregion

    //region Barcode CheckBox
    public void setEnableBarcode(boolean enable) {
        if (enableBarcode != null) {
            this.enableBarcode.setEnabled(enable);
        }
    }

    public boolean isEnableBarcode() {
        if (enableBarcode == null) {
            LoggerUtil.error(TAG, "Barcode handle is null");
            return false;
        }

        return enableBarcode.isChecked();
    }
    //endregion

    //region Tag Operation Button
    public void tagOpBtnEdit(boolean enable) {
        if (enable && !initTagOperationFlag) {
            initTagOperation();
        }
        tagOperationButton.setEnabled(enable);
        tagOperationButton.setClickable(enable);
    }
    //endregion

    //region Beeper
    public void beepCheckBoxEdit(boolean enable) {
        checkBeep.setEnabled(enable);
        checkBeep.setClickable(enable);
    }
    //endregion

    //region Save Button
    public void saveTagBtnEdit(boolean enable) {
        saveTagButton.setEnabled(enable);
        saveTagButton.setClickable(enable);
    }

    public void showSaveTagBtn(boolean show) {
        saveTagButton.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
    //endregion

    //region Region
    public void regionListEdit(boolean enable) {
        regionList.setEnabled(enable);
    }

    public String getSelectedRegionList() {
        return regionList.getSelectedItem().toString();
    }
    //endregion

    //region Temperature Text
    public void setTemperatureText(String temperature) {
        temperatureView.setText(temperature);
    }

    public void setTemperatureText(Spanned fromHtml) {
        temperatureView.setText(fromHtml);
    }
    //endregion

    //region Max Read Rate
    public void setMaxReadRate(String maxReadRate) {
        maxReadRateView.setText(maxReadRate);
    }

    public void setMaxReadRate(Spanned fromHtml) {
        maxReadRateView.setText(fromHtml);
    }
    //endregion

    //region Average Read Rate
    public void setAvgReadText(String avgRead) {
        avgReadView.setText(avgRead);
    }

    public void setAvgReadText(Spanned fromHtml) {
        avgReadView.setText(fromHtml);
    }
    //endregion

    //region Total Tag Count
    public void setTotalTagCountText(String count) {
        totalTagCountView.setText(count);
    }

    public void setTotalTagCountText(Spanned fromHtml) {
        totalTagCountView.setText(fromHtml);
    }
    //endregion

    //region Read Time
    public void setReadingTimeText(String time) {
        readingTimeView.setText(time);
    }

    public void setReadingTimeText(Spanned fromHtml) {
        readingTimeView.setText(fromHtml);
    }
    //endregion

    //region Unique Tag
    public void setUniqueTagText(String count) {
        uniqueTagView.setText(count);
    }

    public void setUniqueTagText(Spanned fromHtml) {
        uniqueTagView.setText(fromHtml);
    }
    //endregion

    //region Error Message
    public void setErrorMsg(String msg) {
        errorMsgView.setText(msg);
    }

    public void setErrorMsgTextColor(int color) {
        errorMsgView.setTextColor(color);
    }

    public void showErrorMsg(boolean show) {
        errorMsgView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
    //endregion

    public void showLayoutFirmware(boolean show) {
        layout_firmware.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showFirmwareHint(boolean show) {
        firmware_hint.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    //endregion

    private void createBeeper() {
        _beeper = Beeper.getInstance();
        if (checkBeep != null) {
            _beeper.setCanBeep(checkBeep.isChecked());
        }
    }

    //region Register Receiver
    private void scanServiceReceiverCtrl(boolean enable) {
        if (enable) {
            IntentFilter scannerIntentFilter = new IntentFilter();
            scannerIntentFilter.addAction(BARCODE_DATA_ACTION);
            scannerIntentFilter.addAction(BARCODE_DATA_LENGTH_ACTION);
            registerReceiver(scanServiceReceiver, scannerIntentFilter);
        } else {
            unregisterReceiver(scanServiceReceiver);
        }
    }

    private void localReceiverCtrl(boolean enable) {
        if (enable) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Action_SelectEPC);
            LocalBroadcastManager.getInstance(this).registerReceiver(mLocalBroadcastReceiver, filter);
        } else {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocalBroadcastReceiver);
        }
    }

    private void systemReceiverCtrl(boolean enable) {
        if (enable) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ActionName.GUN_DETECT);
            registerReceiver(systemServiceReceiver, filter);
        } else {
            unregisterReceiver(systemServiceReceiver);
        }
    }
    //endregion

    /**
     * Set the key code of SCAN_GUN
     */
    private void setGunForRFID() {
        if (remapThread.getStatus() != AsyncTask.Status.RUNNING) {
            if (remapThread.getStatus() == AsyncTask.Status.FINISHED) {
                remapThread = new SetGunKeyViaResumePauseThread("Switching gun key...", this);
            } else {
                remapThread.setMessage("Switching gun key...");
            }
            remapThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            remapThread.execute();
        }
    }

    private void setTargetKey(boolean forRFID) {
        LoggerUtil.error(TAG, "Set gun key to " + forRFID);
        this._targetGunForRFID = forRFID;
    }

    private boolean isTargetGunForRFID() {
        return this._targetGunForRFID;
    }

    private void setGunKeyViaBarcodeCheckBox(boolean isExtended) {
        SetGunKeyViaBarcodeCheckBoxThread newRemapThread = new SetGunKeyViaBarcodeCheckBoxThread("Switching gun key...", this, isExtended);
        newRemapThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        newRemapThread.execute();
    }

    private class SetGunKeyViaResumePauseThread extends AsyncTask<Void, Void, String> {
        private final String TAG = SetGunKeyViaResumePauseThread.class.getSimpleName();
        private ReaderActivity mReaderActivity;
        private String _message;

        SetGunKeyViaResumePauseThread(ReaderActivity activity) {
            this.mReaderActivity = activity;
        }

        SetGunKeyViaResumePauseThread(String message, ReaderActivity activity) {
            this.mReaderActivity = activity;
            this._message = message;
        }

        @Override
        protected void onPreExecute() {
            LoggerUtil.error(TAG, "+++ SetGunKeyViaResumePauseThread");
            keyRemapDialog.setMessage(_message);
            keyRemapDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String exception = "Exception :";
            boolean keyType = false;

            do {
                if (mReaderActivity.firstExecute) {
                    if (!exportProgrammableKey()) {
                        LoggerUtil.error(TAG, "Export programmable key fail\r\n");
                        exception += "Export programmable key fail\r\n";
                        return exception;
                    }
                } else {
                    if (!isKeyConfigFileExist()) {
                        LoggerUtil.debug(TAG, "Does not find key config file, going to export...");

                        if (!exportProgrammableKey()) {
                            LoggerUtil.error(TAG, "Export programmable key fail\r\n");
                            exception += "Export programmable key fail\r\n";
                            return exception;
                        }
                    }
                }

                keyType = isTargetGunForRFID();
                boolean enableKeyRemap = false;

                if (!keyType && onPauseTrigger) {
                    Log.v(TAG, "Will set Key Remap to default value : " + defaultKeyRemapEnable);
                    enableKeyRemap = defaultKeyRemapEnable;
                } else {
                    Log.v(TAG, "Will enable Key Remap");
                    enableKeyRemap = true;
                }

                if (!setKeyRemapStatus(enableKeyRemap)) {
                    exception += "Set key remap status fail\r\n";
                    return exception;
                }

                if (!setTriggerKeyCode(keyType)) {
                    exception += "Set trigger key code fail\r\n";
                    return exception;
                }
            } while (keyType != isTargetGunForRFID());

            return exception;
        }

        @Override
        protected void onPostExecute(String exception) {
            keyRemapDialog.dismiss();

            if (isTargetGunForRFID()) {
                initReader();

                if (m6eReader.getConnectState() == ConnectState.Connecting ||
                        m6eReader.getConnectState() == ConnectState.Disconnecting) {
                    m6eReader.cancel();
                }

                //Waiting connection status to disconnect
                TimeoutCtrl timeoutCtrl = new TimeoutCtrl(DEFAULT_TIMEOUT);

                while (m6eReader.getConnectState() != ConnectState.Disconnected) {
                    sleep(100);
                    if (timeoutCtrl.isTimeout()) {
                        LoggerUtil.error(TAG, "Waiting disconnected timeout");
                        break;
                    }
                }

                onReaderConnectStateChanged(m6eReader, ConnectState.Disconnected, null);

                connectButton.performClick();
            }
        }

        public void setMessage(String message) {
            _message = message;
        }
    }

    private class SetGunKeyViaBarcodeCheckBoxThread extends AsyncTask<Void, Void, String> {
        private final String TAG = SetGunKeyViaBarcodeCheckBoxThread.class.getSimpleName();
        private ReaderActivity mReaderActivity;
        private String _message = "";
        private boolean isExtended = false;

        SetGunKeyViaBarcodeCheckBoxThread(String message, ReaderActivity activity, boolean extended) {
            this.mReaderActivity = activity;
            this._message = message;
            isExtended = extended;
        }

        @Override
        protected void onPreExecute() {
            LoggerUtil.error(TAG, "+++ SetGunKeyViaBarcodeCheckBoxThread");
            keyRemapDialog.setMessage(_message);
            keyRemapDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String exception = "Exception :";

            if (!isKeyConfigFileExist()) {
                LoggerUtil.debug(TAG, "Does not find key config file, going to export...");

                if (!exportProgrammableKey()) {
                    LoggerUtil.error(TAG, "Export programmable key fail\r\n");
                    exception += "Export programmable key fail\r\n";
                    return exception;
                }
            }

            if (!setKeyRemapStatus(true)) {
                exception += "Set key remap status fail\r\n";
                return exception;
            }

            if (!setTriggerKeyCode(isExtended)) {
                exception += "Set trigger key code fail\r\n";
            }

            return exception;
        }

        @Override
        protected void onPostExecute(String exception) {
            keyRemapDialog.dismiss();
        }
    }

    /**
     * Get the Key Remap Enable setting and RFID Trigger key code
     * The library will export the key config setting.
     *
     * @return True for success, false for fail.
     */
    private boolean exportProgrammableKey() {
        try {
            keySetting = KeySetting.getInstance(getApplicationContext());

            if (firstExecute) {
                //Get Key Remap Enable setting
                Bundle result = keySetting.getKeyRemapStatus();
                defaultKeyRemapEnable = result.getBoolean(KeySetting.BUNDLE_KEY_REMAPPING);

                //Get RFID Trigger Key Code
                result = keySetting.getRFIDTriggerKeyCode();
                KEYCODE_SCAN_EXTENDED = Integer.valueOf(result.getString(KeySetting.BUNDLE_KEY_CODE));

                firstExecute = false;
                LoggerUtil.debug(TAG, "Get Default Key Remap Setting : " + defaultKeyRemapEnable);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean setKeyRemapStatus(boolean enableSetting) {
        try {
            if (getTopActivityName().equalsIgnoreCase(PACKAGE_NAME)) {
                if (!onPauseTrigger) {
                    LoggerUtil.debug(TAG, "onPauseTrigger is false, skipCount = 1");
                    skipCount = 1;
                }
            }

            LoggerUtil.debug(TAG, "Set key remap : " + enableSetting + "\r\n");

            Bundle result = keySetting.setKeyRemapStatus(enableSetting);

            if (result.getInt(BUNDLE_ERROR_CODE) != RESULT_CODE_SUCCESS) {
                LoggerUtil.error(TAG, "Set key remap status fail : " + result.getString(BUNDLE_ERROR_MSG) + "\r\n");
                return false;
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.error(TAG, "Set key remap status exception\r\n" + e.getMessage());
            return false;
        }
    }

    private boolean setTriggerKeyCode(boolean isExtended) {
        try {
            if (getTopActivityName().equalsIgnoreCase(PACKAGE_NAME)) {
                if (!onPauseTrigger) {
                    LoggerUtil.debug(TAG, "onPauseTrigger is false, skipCount = 1");
                    skipCount = 1;
                }
            }

            LoggerUtil.debug(TAG, "Set trigger key : " + isExtended + "\r\n");

            Bundle result = keySetting.changeTriggerKeyMode(isExtended);

            if (result.getInt(BUNDLE_ERROR_CODE) != RESULT_CODE_SUCCESS) {
                LoggerUtil.error(TAG, "Set trigger key code fail : " + result.getString(BUNDLE_ERROR_MSG) + "\r\n");
                return false;
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.error(TAG, "Set trigger key code exception\r\n" + e.getMessage());
            return false;
        }
    }

    private boolean isKeyConfigFileExist() {
        File fileKeyConfig = new File(FILE_PATH_KEYS_CONFIG);

        return fileKeyConfig.exists();
    }

    private String getTopActivityName() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName componentInfo = taskInfo.get(0).topActivity;

//        Log.e(TAG, "Top Activity Class Name: " + taskInfo.get(0).topActivity.getClassName());
        LoggerUtil.debug(TAG, "Top Activity Package Name: " + taskInfo.get(0).topActivity.getPackageName());

        return taskInfo.get(0).topActivity.getPackageName();
    }

    private String getLauncherName() {

        PackageManager localPackageManager = getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");

//        List<ResolveInfo> lst = getPackageManager().queryIntentActivities(intent, 0);
//        if (!lst.isEmpty()) {
//            for (ResolveInfo resolveInfo : lst) {
//                Log.d("Test", "New Launcher Found: " + resolveInfo.activityInfo.packageName);
//            }
//        }
//
        String str = localPackageManager.resolveActivity(intent,
                PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
        LoggerUtil.debug(TAG, "Current launcher Package Name:" + str);

        return str;
    }

    //region Receiver
    private BroadcastReceiver scanServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle;
            if (intent != null) {
                if (intent.getAction() == null)
                    return;

                switch (intent.getAction()) {
                    case BARCODE_DATA_ACTION:
                        //收到條碼資料字串
                        bundle = intent.getExtras();
                        if (bundle != null) {
                            String barcodeData = bundle.getString("text");//取得資料字串
                            LoggerUtil.debug(TAG, String.format("Recv scan str: %s", barcodeData));
                            barcodeText.append(barcodeData);
                        }
                        break;
                    case BARCODE_DATA_LENGTH_ACTION:
                        //收到條碼資料長度
                        bundle = intent.getExtras();
                        if (bundle != null) {
                            int len = bundle.getInt("text");//取得資料長度
                            LoggerUtil.debug(TAG, String.format("Recv scan len: %d", len));
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver mLocalBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent mIntent) {
            String action = mIntent.getAction();
            Bundle bundle = mIntent.getExtras();
            if (action == null) return;
            if (bundle == null) return;
            switch (action) {
                case Action_SelectEPC: {
                    String epc = bundle.getString("epc");

                    if (getReadBtnText().equals(getString(R.string.start_reading)) &&
                            m6eReader.isConnected() &&
                            getConnectBtnText().equals(getString(R.string.Disconnect)) &&
                            serviceListener != null
                    ) {
                        targetEPC = epc;
                        sectionsStateAdapter.SetEPCString(epc);
//                        sectionsStateAdapter.notifyDataSetChanged();
                        showSettingsOrTagsView(2);
                    }
                    break;
                }
            }
        }
    };

    private final BroadcastReceiver systemServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle bundle = intent.getExtras();
            if (action == null) return;
            if (bundle == null) return;
            switch (action) {
                case ActionName.GUN_DETECT: {
                    boolean status = bundle.getInt(ExtraName.Gun_State) == 1;
                    LoggerUtil.error(TAG, "Gun state: " + status);

                    if (m6eReader == null) {
                        initReader();
                    }

                    m6eReader.setGunState(status ?  GunState.Attached : GunState.Unattached);

                    if (status) {
                        if (isEnableBarcode()) {
                            return;
                        }

                        TimeoutCtrl timeoutCtrl = new TimeoutCtrl(DEFAULT_TIMEOUT);

                        while (m6eReader.getConnectState() != ConnectState.Disconnected) {
                            sleep(100);
                            if (timeoutCtrl.isTimeout()) {
                                LoggerUtil.error(TAG, "Waiting disconnected timeout");
                                return;
                            }
                        }
                        m6eReader.connect(getSelectedRegionList());
                    } else {
                        switch (m6eReader.getConnectState()) {
                            case Disconnected:
                                break;
                            case Disconnecting:
                            case Connecting:
                                m6eReader.cancel();
                                break;
                            case Connected:
                                m6eReader.disconnect();
                                break;
                        }
                    }
                    break;
                }
            }
        }
    };
    //endregion

    //region Reader Control

    /**
     * Setup RFID reader with properties
     */
    public void setupReader() {
        int async_on = 150;
        int async_off = 100;
        try {
//            async_on = Integer.parseInt(antennaOnTimeView.getText().toString());
//            async_off = Integer.parseInt(antennaOffTimeView.getText().toString());

            async_on = properties.getAntennaOnTime();
            async_off = properties.getAntennaOffTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
        setupReader(async_on, async_off);
    }

    /**
     * Setup RFID reader with properties
     *
     * @param onTime  Antenna on time
     * @param offTime Antenna off time
     */
    public void setupReader(int onTime, int offTime) {
        try {
            //region Set antenna power
            int powerLevel = (properties.getPower() + 10) * 100;
            setParamToReader("/reader/radio/readPower", powerLevel);
            //endregion

            //region Set read timeout
            setParamToReader("/reader/commandTimeout", properties.getReaderTimeout());
            //endregion

            //region Set BLF
            switch (properties.getBLF()) {
                default:
                case BLF_250K:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_BLF, Gen2.LinkFrequency.LINK250KHZ);
                    break;
                case BLF_640K:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_BLF, Gen2.LinkFrequency.LINK640KHZ);
                    break;
            }
            //endregion

            //region Set Session
            switch (properties.getSession()) {
                default:
                case Session_S0:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_SESSION, Gen2.Session.S0);
                    break;
                case Session_S1:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_SESSION, Gen2.Session.S1);
                    break;
                case Session_S2:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_SESSION, Gen2.Session.S2);
                    break;
                case Session_S3:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_SESSION, Gen2.Session.S3);
                    break;
            }
            //endregion

            //region Set Encoding
            switch (properties.getEncoding()) {
                default:
                case Encoding_FM0:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TAGENCODING, Gen2.TagEncoding.FM0);
                    break;
                case Encoding_M2:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TAGENCODING, Gen2.TagEncoding.M2);
                    break;
                case Encoding_M4:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TAGENCODING, Gen2.TagEncoding.M4);
                    break;
                case Encoding_M8:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TAGENCODING, Gen2.TagEncoding.M8);
                    break;
            }
            //endregion

            //region Set Target
            switch (properties.getTarget()) {
                default:
                case Target_AB:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARGET, Gen2.Target.AB);
                    break;
                case Target_BA:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARGET, Gen2.Target.BA);
                    break;
                case Target_A:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARGET, Gen2.Target.A);
                    break;
                case Target_B:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARGET, Gen2.Target.B);
                    break;
            }
            //endregion

            //region Set Q
            switch (properties.getQValue()) {
                default:
                case 0:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_Q, new Gen2.DynamicQ());
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_Q, new Gen2.StaticQ(properties.getQValue() - 1));
                    break;
            }
            //endregion

            //region Set TARI value
            switch (properties.getTARI()) {
                default:
                case TARI_25:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARI, Gen2.Tari.TARI_25US);
                    break;
                case TARI_12_5:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARI, Gen2.Tari.TARI_12_5US);
                    break;
                case TARI_6_25:
                    setParamToReader(TMConstants.TMR_PARAM_GEN2_TARI, Gen2.Tari.TARI_6_25US);
                    break;
            }
            //endregion

            setParamToReader("/reader/read/asyncOnTime", onTime);
            setParamToReader("/reader/read/asyncOffTime", offTime);

            setParamToReader(TMConstants.TMR_PARAM_READER_STATUS_TEMPERATURE, true);

            setParamToReader(TMConstants.TMR_PARAM_TAGOP_ANTENNA, 1);

            SimpleReadPlan simplePlan;
            if (properties.getFastSearch()) {
                LoggerUtil.debug(TAG, "Fast search is On");
                simplePlan = new SimpleReadPlan(new int[]{1}, TagProtocol.GEN2, true);
            } else {
                LoggerUtil.debug(TAG, "Fast search is Off");
                simplePlan = new SimpleReadPlan(new int[]{1}, TagProtocol.GEN2, false);
            }
            m6eReader.paramSet(TMConstants.TMR_PARAM_READ_PLAN, simplePlan);
        } catch (Exception ex) {
            LoggerUtil.error(TAG, "Exception : ", ex);
        }

    }

    public void setupReaderForLocate() {
        try {
            //region Set antenna power
            setParamToReader("/reader/radio/readPower", 2600);
            //endregion

            //region Set read timeout
            setParamToReader("/reader/commandTimeout", 1000);
            //endregion

            //region Set BLF
            setParamToReader(TMConstants.TMR_PARAM_GEN2_BLF, Gen2.LinkFrequency.LINK640KHZ);
            //endregion

            //region Set Session
            setParamToReader(TMConstants.TMR_PARAM_GEN2_SESSION, Gen2.Session.S0);
            //endregion

            //region Set Encoding
            setParamToReader(TMConstants.TMR_PARAM_GEN2_TAGENCODING, Gen2.TagEncoding.FM0);
            //endregion

            //region Set Target
            setParamToReader(TMConstants.TMR_PARAM_GEN2_TARGET, Gen2.Target.A);
            //endregion

            //region Set Q
            setParamToReader(TMConstants.TMR_PARAM_GEN2_Q, new Gen2.DynamicQ());
            //endregion

            //region Set TARI value
            setParamToReader(TMConstants.TMR_PARAM_GEN2_TARI, Gen2.Tari.TARI_6_25US);
            //endregion

            setParamToReader("/reader/read/asyncOnTime", 3000);
            setParamToReader("/reader/read/asyncOffTime", 300);

            setParamToReader(TMConstants.TMR_PARAM_READER_STATUS_TEMPERATURE, true);

            setParamToReader(TMConstants.TMR_PARAM_TAGOP_ANTENNA, 1);

            SimpleReadPlan simplePlan;
            simplePlan = new SimpleReadPlan(new int[]{1}, TagProtocol.GEN2, false);
            m6eReader.paramSet(TMConstants.TMR_PARAM_READ_PLAN, simplePlan);
        } catch (Exception ex) {
            LoggerUtil.error(TAG, "Exception : ", ex);
        }

    }

    public void setTargetTag(String epc) throws ReaderException {
        if (m6eReader == null) {
            LoggerUtil.error(TAG, "Reader is null");
            return;
        }
        SimpleReadPlan simplePlan;
        TagFilter filter = new TagData(epc);
        simplePlan = new SimpleReadPlan(new int[]{1}, TagProtocol.GEN2, filter, 100, false);
        m6eReader.paramSet(TMConstants.TMR_PARAM_READ_PLAN, simplePlan);
    }

    public boolean setParamToReader(String key, Object value) {
        if (m6eReader == null) {
            LoggerUtil.error(TAG, "Reader is null");
            return false;
        }

        Bundle result = m6eReader.setParam(key, value);

        return result.getInt(BUNDLE_ERROR_CODE) == RESULT_CODE_SUCCESS;
    }

    public void updateRFIDVersion() {
        layout_firmware.setVisibility(View.VISIBLE);
        firmware_hint.setVisibility(View.GONE);

        String software = (String) m6eReader.paramGet("/reader/version/software");
        softwareVersionView.setText(software);
        String hardware = (String) m6eReader.paramGet("/reader/version/hardware");
        hardwareVersionView.setText(hardware);
        String model = (String) m6eReader.paramGet("/reader/version/model");
        modelVersionView.setText(model);
        String productGroup = (String) m6eReader.paramGet("/reader/version/productGroup");
        productGroupVersionView.setText(productGroup);
        String serial = (String) m6eReader.paramGet("/reader/version/serial");
        serialNumberView.setText(serial);

        String baudrate = m6eReader.paramGet(TMConstants.TMR_PARAM_BAUDRATE).toString();
        String temperature = m6eReader.paramGet(TMConstants.TMR_PARAM_RADIO_TEMPERATURE).toString();

        LoggerUtil.debug(TAG, String.format("/reader/version/software = %s", software));
        LoggerUtil.debug(TAG, String.format("/reader/version/hardware = %s", hardware));
        LoggerUtil.debug(TAG, String.format("/reader/version/model = %s", model));
        LoggerUtil.debug(TAG, String.format("/reader/version/productGroup = %s", productGroup));
        LoggerUtil.debug(TAG, String.format("/reader/version/serial = %s", serial));

        LoggerUtil.debug(TAG, String.format("%s = %s", TMConstants.TMR_PARAM_BAUDRATE, baudrate));
        LoggerUtil.debug(TAG, String.format("%s = %s", TMConstants.TMR_PARAM_RADIO_TEMPERATURE, temperature));

    }
}

