package com.example.ui_login.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.ui_login.R;
import com.example.ui_login.api.ApiService;
import com.example.ui_login.api.RetrofitClient;
import com.example.ui_login.models.Stores;
import com.example.ui_login.models.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Single extends AppCompatActivity {
    Toolbar toolbar;
    private TextView dateTimeTextView;
    private SimpleDateFormat dateFormat;
    private Calendar calendar;
    private Handler handler;
    private static final String SHARED_PREF_NAME = "dataLogin";
    private SharedPreferences sharedPreferences;
    public static final String API_KEY = "token";
    private Button btnStore;


    AutoCompleteTextView autoCompleteTextView;
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

        btnStore = findViewById(R.id.btnStore);
        btnStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Single.this, Inventory_Store.class);
                startActivity(intent);
            }
        });

        dateTimeTextView = (TextView) findViewById(R.id.date_inventory);
        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        calendar = Calendar.getInstance();
        handler = new Handler();

        // Update the date and time every second
        handler.post(updateTimeTask);

        toolbar = findViewById(R.id.toolbarInventory);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inventory");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        autoCompleteTextView = findViewById(R.id.ListStore);
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_item, new ArrayList<String>());
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setThreshold(0);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
            }
        });


        UserProfile();
        ListStores();
    }

    private void ListStores() {
        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<List<Stores>> call = apiService.getListStore(AuthToken);
        call.enqueue(new Callback<List<Stores>>() {
            @Override
            public void onResponse(Call<List<Stores>> call, Response<List<Stores>> response) {
                if (response.isSuccessful()){
                    List<String> Store = new ArrayList<>();
                    for (Stores stores : response.body()){
                        Store.add(stores.getName());
                    }
                    arrayAdapter.clear();
                    arrayAdapter.addAll(Store);
                    arrayAdapter.notifyDataSetChanged();
                    if (!Store.isEmpty()){
//                        autoCompleteTextView.setText(Store.get(0));
                    }
                } else {
                    Toast.makeText(Single.this, "Call API Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Stores>> call, Throwable t) {
                Toast.makeText(Single.this, "Call API Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void UserProfile() {
        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<User> call = apiService.UserProfile(AuthToken);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    if (user != null) {
                        String name = user.name;
                        String internalId = user.internalId;
                        TextView txtName = findViewById(R.id.employee);
                        TextView txtInternalId = findViewById(R.id.emp_id);
                        txtName.setText(name);
                        txtInternalId.setText(internalId);
                    }
                } else {
                    Toast.makeText(Single.this, "Call API Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(Single.this, "Call API Error", Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private Runnable updateTimeTask = new Runnable() {
        public void run() {
            calendar.setTimeInMillis(System.currentTimeMillis());
            dateTimeTextView.setText(dateFormat.format(calendar.getTime()));
            handler.postDelayed(this, 1000);
        }
    };
}


