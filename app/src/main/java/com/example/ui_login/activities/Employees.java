package com.example.ui_login.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.ui_login.R;
import com.example.ui_login.api.ApiService;
import com.example.ui_login.api.RetrofitClient;
import com.example.ui_login.models.UserEmp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Employees extends AppCompatActivity {

    private static final String SHARED_PREF_NAME = "dataLogin";
    private SharedPreferences sharedPreferences;
    public static final String API_KEY = "token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.tlbEmployees);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        UserEmpProfile();

    }
    public void UserEmpProfile() {
        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<UserEmp> call = apiService.UserEmpProfile(AuthToken);
        call.enqueue(new Callback<UserEmp>() {
            @Override
            public void onResponse(Call<UserEmp> call, Response<UserEmp> response) {
                if (response.isSuccessful()) {
                    UserEmp userEmp = response.body();
                    if (userEmp != null) {
                        String name = userEmp.name;
                        String timeCheckIn = userEmp.shift.timeCheckIn;
                        String timeCheckOut = userEmp.shift.timeCheckOut;
                        TextView txtName = findViewById(R.id.nameEpl);
                        TextView txtIn = findViewById(R.id.TimeIn);
                        TextView txtOut = findViewById(R.id.TimeEnd);
                        txtName.setText(name);
                        txtIn.setText(timeCheckIn);
                        txtOut.setText(timeCheckOut);

                    }
                } else {
                    Toast.makeText(Employees.this, "Call API Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UserEmp> call, Throwable t) {
                Toast.makeText(Employees.this, "Call API Error!!!", Toast.LENGTH_SHORT).show();
            }

        });

    }
    //back Home
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}