package com.example.ui_login.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ui_login.R;
import com.example.ui_login.adapters.CategoryAdapter;
import com.example.ui_login.api.ApiService;
import com.example.ui_login.api.RetrofitClient;
import com.example.ui_login.models.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity {
    Toolbar toolbar;
    private SearchView searchView;
    private CategoryAdapter categoryAdapter;
    private static final String SHARED_PREF_NAME = "dataLogin";
    private SharedPreferences sharedPreferences;
    public static final String API_KEY = "token";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        toolbar = findViewById(R.id.toolbarCategory);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Category");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<List<Category>> call = apiService.getCategories(AuthToken);
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    progressDialog.show();
                    List<Category> categories = response.body();
                    CategoryAdapter categoryAdapter = new CategoryAdapter(categories);
                    RecyclerView categoryRecyclerView = findViewById(R.id.categoryRecyclerView);
                    categoryRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
                    categoryRecyclerView.setAdapter(categoryAdapter);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Toast.makeText(CategoryActivity.this, "Call API Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

/**
 * https://stackoverflow.com/questions/14118309/how-to-use-search-functionality-in-custom-list-view-in-android
 * search list
 */
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        searchView = (SearchView) menu.findItem(R.id.search_category).getActionView();
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                categoryAdapter.getFilter().filter(query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                categoryAdapter.getFilter().filter(newText);
//                return false;
//            }
//        });
//        return true;
//    }


}
