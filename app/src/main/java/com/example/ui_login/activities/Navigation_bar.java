package com.example.ui_login.activities;


import static com.example.ui_login.R.id.drawer_layout;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.ui_login.R;
import com.example.ui_login.api.ApiService;
import com.example.ui_login.api.RetrofitClient;
import com.example.ui_login.models.User;
import com.example.ui_login.models.UserToken;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Navigation_bar extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = "MyActivity";
    Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    private static final String SHARED_PREF_NAME = "dataLogin";
    private static final String KEY_USER = "user";
    private static final String KEY_PASSWORD = "password";
    public static final String API_KEY = "token";
    private static final String KEY_CHECK = "check";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private FloatingActionButton floatingActionButton;


    @SuppressLint("MissingInflatedId")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_bar);

        Button btnStart = (Button) findViewById(R.id.start);
        Button btnInventory = (Button) findViewById(R.id.inventory);
        Button btnCategory = (Button) findViewById(R.id.category);
        Button btnAuto = (Button) findViewById(R.id.auto);
        Button btnSearch = (Button) findViewById(R.id.search_store);
        Button btnEmployees = (Button) findViewById(R.id.employees);

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        String User = sharedPreferences.getString(KEY_USER, null);
        String password = sharedPreferences.getString(KEY_PASSWORD, null);

        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(drawer_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        //bottomNavigation
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.account:
                        Toast.makeText(getApplicationContext(), "Account", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.help:
                        Toast.makeText(getApplicationContext(), "Help", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.notification:
                        Toast.makeText(getApplicationContext(), "Notification", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = findViewById(R.id.scant);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_bar.this, Scant_QR_Code.class);
                startActivity(intent);
            }
        });

        // 6 button
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation_bar.this, ReaderActivity.class);
                startActivity(intent);
            }
        });
        btnInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Navigation_bar.this, Single.class);
                startActivity(intent1);
            }
        });
        btnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(Navigation_bar.this, CategoryActivity.class);
                startActivity(intent2);
            }
        });
        btnAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(Navigation_bar.this, Auto.class);
                startActivity(intent3);
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent(Navigation_bar.this, Search_Store.class);
                startActivity(intent4);
            }
        });
        btnEmployees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent(Navigation_bar.this, Employees.class);
                startActivity(intent4);
            }
        });

        // navigationView
        navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        UserProfile();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_history:
                Toast.makeText(getApplicationContext(), "History", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_help:
                Toast.makeText(getApplicationContext(), "Help", Toast.LENGTH_SHORT).show();
                break;
            case R.id.darkMode:
                startActivity(new Intent(Navigation_bar.this, DarkMode.class));
                break;
            case R.id.nav_myprofile:
                Toast.makeText(getApplicationContext(), "My Profile", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_logout:
                logoutMenu(Navigation_bar.this);
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //navigation view User
    public void UserProfile() {
        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<User> call = apiService.UserProfile(AuthToken);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    if (user != null) {
                        String name = user.name;
                        String phone = user.phone;
                        navigationView = findViewById(R.id.navigationView);
                        View headerView = navigationView.getHeaderView(0);
                        TextView txtName = headerView.findViewById(R.id.txtName);
                        TextView txtPhone = headerView.findViewById(R.id.txtPhone);
                        txtName.setText(name);
                        txtPhone.setText(phone);
                    }
                } else {
                    Toast.makeText(Navigation_bar.this, "Call API Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: Error " + t);

            }

        });
    }

    //Logout

    public void logOutUser() {
        ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
        String AuthToken = "Bearer " + sharedPreferences.getString(API_KEY, "");
        Call<UserToken> call = apiService.SigOut(AuthToken);
        call.enqueue(new Callback<UserToken>() {
            @Override
            public void onResponse(Call<UserToken> call, Response<UserToken> response) {
                SharedPreferences.Editor Editor = sharedPreferences.edit();
                editor.remove(KEY_PASSWORD);
                editor.remove(KEY_CHECK);
                editor.remove(API_KEY);
                editor.apply();
                Toast.makeText(Navigation_bar.this, "Logout Successfully", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Navigation_bar.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<UserToken> call, Throwable t) {
                Toast.makeText(Navigation_bar.this, "Can't Logout", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Show Dialog Logout
    private void logoutMenu(Navigation_bar navigation_bar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(navigation_bar);
        builder.setTitle("LogOut");
        builder.setMessage("Do you want logout ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logOutUser();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}

