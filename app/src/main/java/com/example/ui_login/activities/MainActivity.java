package com.example.ui_login.activities;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ui_login.R;
import com.example.ui_login.api.ApiService;
import com.example.ui_login.api.RetrofitClient;
import com.example.ui_login.databinding.ActivityMainBinding;
import com.example.ui_login.models.LoginActivity;
import com.example.ui_login.models.UserToken;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding Binding;
    private TextView textView;
    private EditText etEmail, etPassword;
    private Button btnLogin;
    private CheckBox checkBox;
    String username, password;
    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static final String SHARED_PREF_NAME = "dataLogin";
    private static final String KEY_USER = "user";
    private static final String KEY_PASSWORD = "password";
    private static final String API_KEY = "token";
    private static final String KEY_CHECK = "check";

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etEmail = findViewById(R.id.etUser);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        checkBox = findViewById(R.id.checkBox);
        textView = findViewById(R.id.textWarring);

        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        //progressbar
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        // callBack User and password
        etEmail.setText(sharedPreferences.getString(KEY_USER, ""));
        etPassword.setText(sharedPreferences.getString(KEY_PASSWORD, ""));
        checkBox.setChecked(sharedPreferences.getBoolean(KEY_CHECK, false));

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
                progressDialog.dismiss();
            }
        });
        checkLogin();
        progressDialog.dismiss();
    }

    private void checkLogin() {
        username = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();

        if (username.isEmpty() && password.isEmpty()) {
            textView.setText("Username and Password is required.");
        } else if (username.isEmpty() || password.isEmpty()) {
            textView.setText("Username or Password is empty.");
        } else {
            boolean loginStatus = LoginSession(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
            if (loginStatus == true) {
                if (checkBox.isChecked()) {
                    editor.putString(KEY_USER, username);
                    editor.putString(KEY_PASSWORD, password);
                    editor.putBoolean(KEY_CHECK, true);
                    editor.commit();
                } else {
                    editor = sharedPreferences.edit();
                    editor.remove(KEY_PASSWORD);
                    editor.remove(KEY_CHECK);
                    editor.commit();
                }
                openNavigation_bar();
                finish();
            } else {
                textView.setText("User or password incorrect. " +
                        "Please check your User or password!!!");
            }
        }
    }

    private void openNavigation_bar() {
        Intent intent = new Intent(MainActivity.this, Navigation_bar.class);
        startActivity(intent);
    }

    private boolean LoginSession(final String username, final String password) {
        boolean retVal = false;
        final Object[] result = {null};
        try {
            Thread thread = new Thread(() -> result[0] = getUser(username, password));
            thread.start();
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result[0] == null) return false;
        Response<UserToken> response = (Response<UserToken>) result[0];

        if (response.isSuccessful()) {
            UserToken userToken = response.body();
            editor.putString(API_KEY, userToken.token);
            Toast.makeText(MainActivity.this, userToken.msg, Toast.LENGTH_SHORT).show();
            retVal = true;
        } else {
            String errMsg = "LOGIN FAILED!!! Error: " + String.valueOf(response.code()) + " - " + response.message();
            retVal = false;
        }

        return retVal;
    }

    private final Response<UserToken> getUser(String username, String password) {
        Response<UserToken> response = null;
        try {
            LoginActivity user = new LoginActivity();
            user.username = username;
            user.password = password;
            ApiService apiService = RetrofitClient.getRetrofitClient().create(ApiService.class);
            Call<UserToken> call = apiService.getUser(user);
            response = call.execute();

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return response;
    }

}