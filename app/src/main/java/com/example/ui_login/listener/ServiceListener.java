package com.example.ui_login.listener;
import static com.example.ui_login.until.Utilities.sleep;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.text.Html;
import android.view.View;

import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.activities.TagRecord;
import com.example.ui_login.enums.ActionState;
import com.example.ui_login.until.LoggerUtil;
import com.example.ui_login.R;
import com.thingmagic.ReadExceptionListener;
import com.thingmagic.ReadListener;
import com.thingmagic.Reader;
import com.thingmagic.ReaderException;
import com.thingmagic.SerialReader;
import com.thingmagic.StatusListener;
import com.thingmagic.TagReadData;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

@SuppressLint("StaticFieldLeak")
public class ServiceListener implements View.OnClickListener {
    private final String TAG = ServiceListener.class.getSimpleName();

    public ReentrantLock lockTags = new ReentrantLock();
    public ConcurrentHashMap<String, TagRecord> epcToReadDataMap = new ConcurrentHashMap<>();
    private double maxReadRate = 0;
    private long queryStartTime = 0;
    private long queryStopTime = 0;

    private ReaderActivity mReaderActivity;

    private InventoryThread inventoryThread;

    public ServiceListener(ReaderActivity readerActivity) {
        mReaderActivity = readerActivity;
        findAllViewsById();
    }

    private void findAllViewsById() {
//        inflater = (LayoutInflater) mReaderActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        errorMsgView = mReaderActivity.findViewById(R.id.error_messages_view);
//        textColor = mReaderActivity.getSearchResultTextColor();
//        table = (TableLayout) mReaderActivity.findViewById(R.id.tablelayout);
    }

    @Override
    public void onClick(View arg0) {
        try {
            if (mReaderActivity.getReadBtnText().equals(mReaderActivity.getResources().getString(R.string.stop_reading))) {
                stopReading();
            } else if (mReaderActivity.getReadBtnText().equals(mReaderActivity.getResources().getString(R.string.start_reading))) {
                startReading();
            }
        } catch (Exception ex) {
            LoggerUtil.error(TAG, "Exception", ex);
        }
    }

    public void startReading() {
        LoggerUtil.debug(TAG, "Trigger start reading");
        if (mReaderActivity.m6eReader != null) {
            inventoryThread = new InventoryThread(mReaderActivity);

            mReaderActivity.m6eReader.addReadListener(inventoryThread);
            mReaderActivity.m6eReader.addStatusListener(inventoryThread);
            mReaderActivity.m6eReader.addReadExceptionListener(inventoryThread);

            mReaderActivity.m6eReader.inventory();

            inventoryThread.execute();
        } else {
            LoggerUtil.error(TAG, "Reader is null");
        }
    }

    public void stopReading() {
        LoggerUtil.debug(TAG, "Trigger stop reading");
        if (mReaderActivity.m6eReader != null) {
            mReaderActivity.m6eReader.removeReadListener(inventoryThread);
            mReaderActivity.m6eReader.removeStatusListener(inventoryThread);
            mReaderActivity.m6eReader.removeReadExceptionListener(inventoryThread);

            if (inventoryThread != null) {
                inventoryThread.stop();
            }

            mReaderActivity.m6eReader.stop();
        } else {
            if (inventoryThread != null) {
                inventoryThread.stop();
            }
        }
    }

    public String listTags() {
//        String tags = "";
//
//        int index = 1;
//        for (TagRecord tag : new ArrayList<>(epcToReadDataMap.values())) {
//            tags += String.format("%s,%s,%s,%s\r\n", index++, tag.getEpcString(), tag.getRssi(), tag.readCount);
//        }

        return mReaderActivity.tagListAdapter.toString();
    }
    //---------------------------------------------------------------------------------------------

    public void clearTagRecords() {
        lockTags.lock();
        epcToReadDataMap.clear();
        lockTags.unlock();
//        table.removeAllViews();
        mReaderActivity.clearInventoryResult();
        maxReadRate = 0;
        queryStartTime = System.currentTimeMillis();
    }

    public class InventoryThread extends AsyncTask<Void, Integer, ConcurrentHashMap<String, TagRecord>> implements ReadExceptionListener, ReadListener, StatusListener {
        private final String TAG = InventoryThread.class.getSimpleName();
        private final ReaderActivity _activity;

        private int count;
        private int _temperature;
        private boolean _stopThread = true;
        private Timer _timer = new Timer();

        private final DecimalFormat df = new DecimalFormat("#.##");

        InventoryThread(ReaderActivity activity) {
            this._activity = activity;
        }

        @Override
        protected void onPreExecute() {
            LoggerUtil.debug(TAG, "+++ InventoryThread");
            clearTagRecords();

            count = 0;
            _temperature = 0;
            _stopThread = false;

            lockTags.lock();
            epcToReadDataMap.clear();
            lockTags.unlock();
        }

        @Override
        protected ConcurrentHashMap<String, TagRecord> doInBackground(Void... voids) {
            LoggerUtil.debug(TAG, "Enter doInBackground()");
            queryStartTime = System.currentTimeMillis();
            queryStopTime = queryStartTime;
            refreshReadRate();
            while (!_stopThread) {
                /* Waiting till stop reading button is pressed */

//                    generateRandomTag(10);

                sleep(5);
            }

            _timer.cancel();

            queryStopTime = System.currentTimeMillis();

            return epcToReadDataMap;
        }

        @Override
        protected void onPostExecute(ConcurrentHashMap<String, TagRecord> tagList) {
            LoggerUtil.debug(TAG, "Enter onPostExecute()");
            _timer.cancel();
            String temp_str;

            lockTags.lock();
            mReaderActivity.UpdateTagList(epcToReadDataMap);
            epcToReadDataMap.clear();
            lockTags.unlock();

            mReaderActivity.setUniqueTagText(Html.fromHtml("<b>Unique Tags :</b> " + mReaderActivity.tagListAdapter.size()));

            mReaderActivity.setTotalTagCountText(Html.fromHtml("<b>Total Tags  :</b> " + mReaderActivity.tagListAdapter.getTotalTags()));

            long elapsedTime = queryStopTime - queryStartTime;
            double timeTaken = ((double) elapsedTime / 1000);
            temp_str = df.format(timeTaken) + " sec";
            mReaderActivity.setReadingTimeText(Html.fromHtml("<b>Time:</b> " + temp_str));
            mReaderActivity.setAvgReadText(Html.fromHtml("<b>Avg. Reads Rate:</b> " + df.format(mReaderActivity.tagListAdapter.getTotalTags() / timeTaken)));

            LoggerUtil.info(TAG, String.format("Unique Tags: %d, Total Tags: %d, Time: %s, Avg. Reads Rate: %s",
                    mReaderActivity.tagListAdapter.size(),
                    mReaderActivity.tagListAdapter.getTotalTags(),
                    temp_str,
                    df.format(mReaderActivity.tagListAdapter.getTotalTags() / timeTaken)
            ));

            LoggerUtil.debug(TAG, "--- InventoryThread");
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected void onProgressUpdate(Integer... progress) {
            lockTags.lock();
            mReaderActivity.UpdateTagList(epcToReadDataMap);
            epcToReadDataMap.clear();
            lockTags.unlock();

            mReaderActivity.setUniqueTagText(Html.fromHtml("<b>Unique Tags :</b> " + mReaderActivity.tagListAdapter.size()));
            mReaderActivity.setTotalTagCountText(Html.fromHtml("<b>Total Tags  :</b> " + mReaderActivity.tagListAdapter.getTotalTags()));

            long elapsedTime = System.currentTimeMillis() - queryStartTime;
            double timeTaken = ((double) elapsedTime / 1000);
            String temp_str = df.format(timeTaken) + " sec";

            mReaderActivity.setReadingTimeText(Html.fromHtml("<b>Time:</b> " + temp_str));

            double tmpAvg = mReaderActivity.tagListAdapter.getTotalTags() / timeTaken;

            if (timeTaken > 1) {
                if (tmpAvg > maxReadRate)
                    maxReadRate = tmpAvg;

                String strMaxReadRate = df.format(maxReadRate);
                mReaderActivity.setMaxReadRate(Html.fromHtml("<b>Max. Read Rate:</b> " + strMaxReadRate));
            }

            String avg_str = df.format(tmpAvg);
            mReaderActivity.setAvgReadText(Html.fromHtml("<b>Avg. Reads Rate:</b> " + avg_str));

            if (++count >= 100) {
                LoggerUtil.info(TAG, String.format("Unique Tags: %d, Total Tags: %d, Time: %s, Avg. Reads Rate: %s",
                        mReaderActivity.tagListAdapter.size(),
                        mReaderActivity.tagListAdapter.getTotalTags(),
                        temp_str,
                        avg_str
                ));
                count = 0;
            }
        }

        private void refreshReadRate() {
            _timer = new Timer();
            _timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    publishProgress(0);
                }
            }, 100, 100);
        }

        public void stop() {
            _stopThread = true;
        }

        @Override
        public void tagReadException(Reader reader, ReaderException e) {
            String message = e.getMessage();
            if (message != null &&
                    (
                            message.contains("The module has detected high return loss") ||
                                    message.contains("Tag ID buffer full") ||
                                    message.contains("No tags found")
                    )
            ) {
                // exception = "No connected antennas found";
                /* Continue reading */
                LoggerUtil.warn(TAG, "Waring exception: " + e);
            } else {
                LoggerUtil.error(TAG, "Reader exception: " + e);
            }

            _activity.m6eReader.onReaderActionChanged(ActionState.Stop, e);

            if (message != null && message.toLowerCase(Locale.ROOT).contains("timeout")) {
                _activity.m6eReader.disconnect();
            }
        }

        @Override
        public void tagRead(Reader reader, TagReadData tagReadData) {
            _activity.m6eReader.onReaderReadTag(tagReadData);
        }

        @Override
        public void statusMessage(Reader reader, SerialReader.StatusReport[] statusReports) {
            for (int i = 0; i < statusReports.length; i++) {
                if (statusReports[i] instanceof SerialReader.TemperatureStatusReport) {
                    int newTemperature = ((SerialReader.TemperatureStatusReport) statusReports[i]).getTemperature();
                    if (newTemperature != _temperature) {
                        LoggerUtil.debug(TAG, statusReports[i].toString());
                        _temperature = newTemperature;
                        _activity.m6eReader.onReaderTemperatureChanged(_temperature);
                    }
                } else if (statusReports[i] instanceof SerialReader.FrequencyStatusReport) {
                    LoggerUtil.debug(TAG, statusReports[i].toString());
                }
            }
        }
    }

}
