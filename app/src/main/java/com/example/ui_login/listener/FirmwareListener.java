package com.example.ui_login.listener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.until.DocumentFileUtils;
import com.example.ui_login.until.LoggerUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by USER
 */
public class FirmwareListener implements View.OnClickListener {

    public static final int FILE_SELECT_CODE = 2891;
    private static final String TAG = FirmwareListener.class.getSimpleName();

    private static ReaderActivity mReaderActivity;
    private static ProgressDialog pDialog;
    private static Button btn_firmware_upgrade = null;
    private static FirmwareUpgradeTask firmwareUpgradeTask;
    private boolean bFirmwareUpgrading;

    public FirmwareListener(ReaderActivity readerActivity) {
        mReaderActivity = readerActivity;

        pDialog = new ProgressDialog(readerActivity);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        findAllViewsById();
    }

    private void findAllViewsById() {
        btn_firmware_upgrade.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.putExtra("android.content.extra.FANCY", true);
        intent.putExtra("android.content.extra.SHOW_FILESIZE", true);

        try {
            this.bFirmwareUpgrading = true;
            mReaderActivity.startActivityForResult(
                    Intent.createChooser(intent, "Select a Firmware to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(mReaderActivity, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onFileSelectorResult(Intent data) {
        // Get the Uri of the selected file
        Uri uri = data.getData();
        LoggerUtil.debug(TAG, "File Uri: " + uri.toString());
        // Get the path
        String path = getPath(mReaderActivity, uri);
        LoggerUtil.debug(TAG, "File Path: " + path);

        if (firmwareUpgradeTask == null || !firmwareUpgradeTask.isExecuting()) {
            firmwareUpgradeTask = new FirmwareUpgradeTask(uri);
            firmwareUpgradeTask.execute();
        } else {
            LoggerUtil.info(TAG, "firmwareUpgradeTask is null or executing");
        }
    }

    public boolean isFirmwareUpgrading() {
        return bFirmwareUpgrading;
    }

    private String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private class FirmwareUpgradeTask extends AsyncTask<Void, Void, Void> {
        private boolean isRunning;
        private Uri uri;
        private boolean hasError;

        private String oldVersion;

        public FirmwareUpgradeTask(Uri uri) {
            this.uri = uri;
            isRunning = false;
            hasError = false;
        }

        @Override
        protected void onPreExecute() {
            pDialog.show();
            oldVersion = (String) mReaderActivity.m6eReader.paramGet("/reader/version/software");

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            isRunning = true;
            firmwareUpgrade();
            isRunning = false;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String newVersion;
            newVersion = (String) mReaderActivity.m6eReader.paramGet("/reader/version/software");

            if (newVersion == null) {
                newVersion = "";
            }

            mReaderActivity.closeSettings();
            bFirmwareUpgrading = false;
            pDialog.dismiss();

            AlertDialog deleteAlert = new AlertDialog.Builder(mReaderActivity).create();
            deleteAlert.setTitle("Firmware Upgrade");
            if (hasError) {
                deleteAlert.setMessage("Upgrade failed.");
            } else {
                deleteAlert.setMessage(String.format("Upgrade completed\nFrom \n%s \nto \n%s", oldVersion, newVersion));
                mReaderActivity.updateRFIDVersion();
            }
            deleteAlert.show();

            TextView textView = deleteAlert.findViewById(android.R.id.message);
            if (textView != null)
                textView.setTextSize(14);


            super.onPostExecute(aVoid);
        }

        private void firmwareUpgrade() {
            String mFilename;
            File file = new File(uri.getPath());
            InputStream iStream = null;
            byte[] tempContentArray = null;

            try {
                if (file.exists()) {
                    iStream = new FileInputStream(file);
                    mFilename = file.getName();
                } else {
                    ContentResolver resolver = mReaderActivity.getContentResolver();
                    iStream = resolver.openInputStream(uri);

                    // http://stackoverflow.com/questions/5568874/how-to-extract-the-file-name-from-uri-returned-from-intent-action-get-content
                    Cursor returnCursor = resolver.query(uri, null, null, null, null);
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    returnCursor.moveToFirst();
                    mFilename = returnCursor.getString(nameIndex);

                    // https://stackoverflow.com/questions/5657411/android-getting-a-file-uri-from-a-content-uri
                    if (mFilename == null) {
                        int pathIndex = returnCursor.getColumnIndex(MediaStore.Images.Media.DATA);
                        int lastIndex = returnCursor.getString(pathIndex).lastIndexOf("/") + 1;
                        mFilename = returnCursor.getString(pathIndex).substring(lastIndex);
                    }
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    long fileSize = returnCursor.getLong(sizeIndex);
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(iStream);
                    tempContentArray = new byte[(int) fileSize];
                    bufferedInputStream.read(tempContentArray);
                    bufferedInputStream.close();
                    iStream = resolver.openInputStream(uri);

                    LoggerUtil.debug(TAG, String.format("Firmware filename: %s", mFilename));

                    String filepath = uri.toString();
                    try {
                        if (uri.getScheme().equals("content")) {
                            // Try to parse the real file path
                            String pathFromUri = DocumentFileUtils.getFullPathFromUri(uri, mReaderActivity);
                            if (!pathFromUri.equals(File.separator)) {
                                filepath = pathFromUri;
                            } else {
                                Cursor cursor = mReaderActivity.getContentResolver().query(uri, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null);
                                cursor.moveToFirst();
                                pathFromUri = cursor.getString(0);
                                cursor.close();

                                if (!pathFromUri.equals(File.separator) && pathFromUri.startsWith(File.separator)) {
                                    filepath = pathFromUri;
                                }
                            }
                        } else if (uri.getScheme().equals("file")) {
                            filepath = uri.getPath();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    LoggerUtil.debug(TAG, String.format("Firmware filepath: %s", filepath));
                    returnCursor.close();
                }

                if (mReaderActivity.m6eReader.isConnected() && iStream != null) {
                    mReaderActivity.m6eReader.firmwareLoad(iStream);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                hasError = true;
            } finally {
                if (iStream != null) {
                    try {
                        iStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public synchronized boolean isExecuting() {
            return isRunning;
        }
    }
}

