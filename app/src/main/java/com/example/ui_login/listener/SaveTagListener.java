package com.example.ui_login.listener;


import static com.example.ui_login.until.Utilities.getTimeFormat;
import static java.lang.Thread.sleep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;

import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.until.LoggerUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SaveTagListener implements View.OnClickListener {

    private final String TAG = SaveTagListener.class.getSimpleName();

    private ProgressDialog pDialog;

    private ReaderActivity activity;

    public SaveTagListener(ReaderActivity activity) {
        this.activity = activity;
        pDialog = new ProgressDialog(this.activity);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onClick(View v) {
        SaveTagThread saveTagThread = new SaveTagThread(activity);
        saveTagThread.execute();
    }

    private class SaveTagThread extends AsyncTask<Void, Void, String> {
        private ReaderActivity mReaderActivity;
        private boolean operationStatus = true;

        SaveTagThread(ReaderActivity activity) {
            this.mReaderActivity = activity;
        }

        @Override
        protected void onPreExecute() {

            pDialog.setMessage("Saving Tags. Please wait...");

            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String exception = "";

            String rootPath = Environment.getExternalStorageDirectory().toString();
            String tagPath = File.separator + "unitech" + File.separator + "RFIDReader";

            File file = new File(rootPath + tagPath);

            if (!mReaderActivity.createDirectory(file.getPath())) {
                LoggerUtil.error(TAG, "Create folder fail.");
                exception = "Create folder fail";
                return exception;
            }

            String data = mReaderActivity.serviceListener.listTags();

            if (data.length() == 0) {
                LoggerUtil.debug(TAG, "No data to save");
                exception = "No data to save";
                return exception;
            }

            String fileName = "RFIDReader" + getTimeFormat("yyyyMMdd_hhmmss_SSS") + ".txt";

            String targetFile = rootPath + tagPath + File.separator + fileName;

            try {
                FileWriter fw = new FileWriter(targetFile);

                fw.write(data);
                fw.flush();

                //Check file write finished
                long tempSize = 0;
                int count = 0;
                long startTime = System.currentTimeMillis();
                File saveFile = new File(targetFile);

                while (true) {
                    if (tempSize != targetFile.length()) {
                        count = 0;
                        tempSize = targetFile.length();
                    } else {
                        if (tempSize != 0)
                            count++;
                    }

                    if (count > 2)
                        break;

                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                LoggerUtil.debug(TAG, "Success! Save to " + fileName);
                exception = "Success! Save to " + fileName;
            } catch (IOException e) {
                LoggerUtil.error(TAG, "Save tag exception : " + e.getMessage());
                exception = "Fail : Save tag exception : " + e.getMessage();
            }

            return exception;
        }

        @Override
        protected void onPostExecute(String exception) {
            pDialog.dismiss();

            if (exception.length() > 0) {
                AlertDialog finishedDlg = new AlertDialog.Builder(mReaderActivity).create();
                finishedDlg.setTitle("Save Tag");
                finishedDlg.setMessage(exception);
                finishedDlg.show();
            }
        }
    }
}
