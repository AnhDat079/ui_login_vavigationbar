package com.example.ui_login.listener;

import android.annotation.SuppressLint;
import android.view.View;

import com.example.ui_login.activities.ReaderActivity;
import com.example.ui_login.R;

@SuppressLint("StaticFieldLeak")
public class ConnectionListener implements View.OnClickListener {
    private static final String TAG = ConnectionListener.class.getSimpleName();
    private final ReaderActivity mReaderActivity;

    public ConnectionListener(ReaderActivity readerActivity) {
        mReaderActivity = readerActivity;
    }

    @Override
    public void onClick(View arg0) {
        mReaderActivity.clearInventoryResult();

        mReaderActivity.initReader();

        if (mReaderActivity.getConnectBtnText().equalsIgnoreCase(mReaderActivity.getString(R.string.Connect))) {

            mReaderActivity.m6eReader.connect(mReaderActivity.getSelectedRegionList());
        } else {
            mReaderActivity.m6eReader.disconnect();
        }
    }
}
