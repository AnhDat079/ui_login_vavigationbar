package com.example.ui_login.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ui_login.R;
import com.example.ui_login.models.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> implements Filterable {
    private List<Category> categories;
    private List<Category> categoriesSearch;


    public CategoryAdapter(List<Category> categories) {
        this.categories = categories;
        this.categoriesSearch = categories;
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        if (categories != null && categories.size() > 0) {
            Category category = categories.get(position);
            holder.internalId.setText(category.getInternalId());
            holder.name.setText(category.name);

//            holder.imagesView.setImageResource(category.images);
        } else {
            return;
        }
    }

    @Override
    public int getItemCount() {
        if (categories != null) {
            return categories.size();
        }
        return 0;
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView internalId, name;
        ImageView imagesView;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            internalId = itemView.findViewById(R.id.id_category);
            name = itemView.findViewById(R.id.title_category);
            imagesView = itemView.findViewById(R.id.img_category);
        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String strSearch = constraint.toString();
                if (strSearch.isEmpty()) {
                    categories = categoriesSearch;
                } else {
                    List<Category> listCategory = new ArrayList<>();
                    for (Category category : categoriesSearch) {
                        if (category.name.toLowerCase().contains(strSearch.toLowerCase())) {
                            listCategory.add(category);
                        }
                    }
                    categories = listCategory;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categories;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                categories = (List<Category>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
