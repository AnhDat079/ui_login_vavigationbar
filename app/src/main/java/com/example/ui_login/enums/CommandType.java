package com.example.ui_login.enums;

public enum CommandType {
    Unknown(-1, "Unknown"),
    Connect(0, "Connect"),
    Disconnect(1, "Disconnect"),
    PowerOn(2, "Power on"),
    PowerOff(3, "Power off"),
    UART(4, "Get UART"),
    ;

    private final int _code;
    private final String _name;

    CommandType(int code, String name) {
        this._code = code;
        this._name = name;
    }

    public int value() {
        return _code;
    }

    public String toString() {
        return _name;
    }

    /**
     * Returns the enum constant of this type with the specified code.
     *
     * @param code The code to check
     * @return The enum constant with the specified code
     */
    public static CommandType valueOf(int code) {
        for (CommandType e : CommandType.values()) {
            if (e.value() == code) {
                return e;
            }
        }

        return Unknown;
    }

    public static CommandType valueOfName(String name) {
        for (CommandType e : CommandType.values()) {
            if (e.toString().equalsIgnoreCase(name)) {
                return e;
            }
        }

        return Unknown;
    }
}

