package com.example.ui_login.enums;

public enum GunState {
    Unknown(0, "Unknown"),
    Attached(1, "Attached"),
    Unattached(2, "Unattached"),
    ;

    private final int _code;
    private final String _name;

    GunState(int code, String name) {
        this._code = code;
        this._name = name;
    }

    @Override
    public String toString() {
        return _name;
    }

    public int value() {
        return _code;
    }

    /**
     * Returns the enum constant of this type with the specified code.
     *
     * @param code The code to check
     * @return The enum constant with the specified code
     */
    public static GunState valueOf(int code) {
        for (GunState e : GunState.values()) {
            if (e.value() == code) {
                return e;
            }
        }

        return Unknown;
    }

    public static GunState valueOfName(String name) {
        for (GunState e : GunState.values()) {
            if (e.toString().equalsIgnoreCase(name)) {
                return e;
            }
        }

        return Unknown;
    }
}
