package com.example.ui_login.enums;

public enum ActionState {
    Stop(0, "Stop"),
    Inventory(1, "Inventory"),
    Inventorying(2, "Inventorying"),
    Stopping(3, "Stopping"),
    ;

    private final int _code;
    private final String _name;

    ActionState(int code, String name) {
        this._code = code;
        this._name = name;
    }


    @Override
    public String toString() {
        return _name;
    }

    public int value() {
        return _code;
    }

    /**
     * Returns the enum constant of this type with the specified code.
     *
     * @param code The code to check
     * @return The enum constant with the specified code
     */
    public static ActionState valueOf(int code) {
        for (ActionState e : ActionState.values()) {
            if (e.value() == code) {
                return e;
            }
        }

        return Stop;
    }

    public static ActionState valueOfName(String name) {
        for (ActionState e : ActionState.values()) {
            if (e.toString().equalsIgnoreCase(name)) {
                return e;
            }
        }

        return Stop;
    }
}

