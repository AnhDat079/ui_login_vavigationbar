package com.example.ui_login.enums;

public enum ConnectState {
    Disconnected(0, "Disconnected"),
    Connecting(1, "Connecting"),
    Connected(2, "Connected"),
    Disconnecting(3, "Disconnecting"),
    ;

    private final int _code;
    private final String _name;

    ConnectState(int code, String name) {
        this._code = code;
        this._name = name;
    }


    @Override
    public String toString() {
        return _name;
    }

    public int value() {
        return _code;
    }

    /**
     * Returns the enum constant of this type with the specified code.
     *
     * @param code The code to check
     * @return The enum constant with the specified code
     */
    public static ConnectState valueOf(int code) {
        for (ConnectState e : ConnectState.values()) {
            if (e.value() == code) {
                return e;
            }
        }

        return Disconnected;
    }

    public static ConnectState valueOfName(String name) {
        for (ConnectState e : ConnectState.values()) {
            if (e.toString().equalsIgnoreCase(name)) {
                return e;
            }
        }

        return Disconnected;
    }
}
